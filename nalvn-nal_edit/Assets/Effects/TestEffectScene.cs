﻿using UnityEngine;
using System.Collections;

public class TestEffectScene : MonoBehaviour {
    string[] effects;
    int curEffectIndex = 0;
	// Use this for initialization
	void Start () {
        effects = new string[] {
            "BlackHoleAnimation",
            "ExposeEffect1",
            "ExposeEffect2",
            "ExposeEffect3",
            "ExposeEffect4",
        };
	}
	
	void OnGUI()
    {
        if(GUI.Button(new Rect(0,0,100,70), "<"))
        {
            curEffectIndex -= 1;
            if (curEffectIndex < 0)
                curEffectIndex = effects.Length - 1;

            AddEffect();
        }

        if (GUI.Button(new Rect(100, 0, 100, 70), effects[curEffectIndex]))
        {
            AddEffect();
        }

        if (GUI.Button(new Rect(200, 0, 100, 70), ">"))
        {
            curEffectIndex += 1;
            if (curEffectIndex >= effects.Length)
                curEffectIndex = 0;

            AddEffect();
        }
    }

    void AddEffect()
    {
        //編集前
        //GameObject obj = (GameObject)Instantiate((GameObject)Resources.Load(effects[curEffectIndex]));

        //編集後
        switch(effects[curEffectIndex])
        {
            case "ExposeEffect1":
            Instantiate(Resources.Load("effect/ChainEffect_test"), new Vector3(0, 0, 0), Quaternion.identity);
                break;
            case "ExposeEffect2":
                Instantiate(Resources.Load("effect/ChainEffect_test2"), new Vector3(0, 0, 0), Quaternion.identity);
                break;
            case "ExposeEffect3":
                Instantiate(Resources.Load("effect/ChainEffect_test3"), new Vector3(0, 0, 0), Quaternion.identity);
                break;
        }
    }
}
