﻿#if UNITY_EDITOR
using UnityEditor;

[InitializeOnLoad]
public class PreloadProject
{
    // Auto insert keystore and keyalias password for build
    static PreloadProject()
    {
		PlayerSettings.Android.keystorePass = "Ak1k0yada";
		PlayerSettings.Android.keyaliasPass = "Ak1k0yada";
    }

}

#endif