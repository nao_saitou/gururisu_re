using System.Collections;
using System;

public class Utilities  {

	public static string GetCurrentDateJapanese()
	{
		return GetDateJapanese (DateTime.Now);
	}

	public static string GetDateJapanese(DateTime date)
	{
		return string.Format("{0}月{1}日", date.Month, date.Day);
	}

    public static string GetChangeTetrisBlock(string type)
    {
        type = type.ToLower();
        string result = "";
        switch(type)
        {
            case "change_i":
                result = "I";
                break;
            case "change_j":
                result = "J";
                break;
            case "change_l":
                result = "L";
                break;
            case "change_t":
                result = "T";
                break;
            case "change_o":
                result = "O";
                break;
            case "change_s":
                result = "S";
                break;
            case "change_z":
                result = "Z";
                break;
        }
        return result;
    }
}
