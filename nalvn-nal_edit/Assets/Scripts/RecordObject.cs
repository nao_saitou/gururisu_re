﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class RecordObject : MonoBehaviour {
	public Text rank;
	public Text date;
	public Text score;

	public void SetText(string _rank, string _date, string _score = "")
	{
		rank.text = _rank;
		date.text = _date;
		score.text = _score;
	}
}
