using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using SimpleLibs;
using UnityEngine.UI;

public class Tetrimo : MonoBehaviour {

	struct FieldMatrixStruct
	{
		public int x;
		public int y;
	}

    #region Shapes
    static Vector2[, ,] Shapes = new Vector2[7,4,4] {
        // # J
        // ###
		{
			{new Vector2(0, 1), new Vector2(0, 0), new Vector2(1, 0), new Vector2(2, 0)},
			{new Vector2(2, 1), new Vector2(1, 1), new Vector2(1, 0), new Vector2(1,-1)},
			{new Vector2(2, -1),new Vector2(2, 0), new Vector2(1, 0), new Vector2(0, 0)},
			{new Vector2(0,-1), new Vector2(1,-1), new Vector2(1, 0), new Vector2(1, 1)},
		},
		// # L
		// ###
		{
			{new Vector2(0, 0), new Vector2(1, 0), new Vector2(2, 0), new Vector2(2, 1)},
			{new Vector2(1, 1), new Vector2(1, 0), new Vector2(1,-1), new Vector2(2,-1)},
			{new Vector2(2, 0), new Vector2(1, 0), new Vector2(0, 0), new Vector2(0, -1)},
			{new Vector2(1,-1), new Vector2(1, 0), new Vector2(1, 1), new Vector2(0, 1)}
        },
        //  # T
        // ###
		{
            {new Vector2(0, 0), new Vector2(1, 0), new Vector2(2, 0), new Vector2(1, 1)},
            {new Vector2(1, 1), new Vector2(1, 0), new Vector2(1,-1), new Vector2(2, 0)},
            {new Vector2(2, 0), new Vector2(1, 0), new Vector2(0, 0), new Vector2(1, -1)},
            {new Vector2(1, -1), new Vector2(1, 0), new Vector2(1, 1), new Vector2(0,0)}
        },
        // ## O
        // ##
		{
            {new Vector2(0, 1), new Vector2(1, 1), new Vector2(0, 0), new Vector2(1, 0)},
			{new Vector2(1, 1), new Vector2(0, 0), new Vector2(1, 0), new Vector2(0, 1)},
			{new Vector2(0, 0), new Vector2(1, 0), new Vector2(0, 1), new Vector2(1, 1)},
			{new Vector2(1, 0), new Vector2(0, 1), new Vector2(1, 1), new Vector2(0, 0)}
        },
        // # I
        // ####
		{
			{new Vector2(-1, 1), new Vector2(0, 1), new Vector2(1, 1), new Vector2(2, 1)},
			{new Vector2(1, 2), new Vector2(1, 1), new Vector2(1, 0), new Vector2(1, -1)},
			{new Vector2(2, 0), new Vector2(1, 0), new Vector2(0, 0), new Vector2(-1, 0)},
			{new Vector2(0, -1), new Vector2(0, 0), new Vector2(0, 1), new Vector2(0, 2)}
		},                                                                           
        //  ##   S                                                                    
        // ##                                                                        
		{                                                                            
            {new Vector2(0, 0), new Vector2(1, 0), new Vector2(1, 1), new Vector2(2, 1)},
            {new Vector2(1, 1), new Vector2(1, 0), new Vector2(2, 0), new Vector2(2,-1)},
			{new Vector2(2, 0), new Vector2(1, 0), new Vector2(1, -1),new Vector2(0, -1)},
			{new Vector2(1,-1), new Vector2(1, 0), new Vector2(0, 0), new Vector2(0, 1)}
        },                                                                           
		// ##    Z                                                                    
        //  ##                                                                       
		{                                                                            
            {new Vector2(0, 1), new Vector2(1, 1), new Vector2(1, 0), new Vector2(2, 0)},
			{new Vector2(2, 1), new Vector2(2, 0), new Vector2(1, 0), new Vector2(1, -1)},
			{new Vector2(2, -1),new Vector2(1, -1),new Vector2(1, 0), new Vector2(0, 0)},
            {new Vector2(0, -1), new Vector2(0, 0), new Vector2(1, 0), new Vector2(1,1)}
        }
	};

	static Vector2[, , ,] SuperRotationShapes = new Vector2[3,4,4,4] {

		// FIRST IS J
		{
			{
				{new Vector2(0, 1), new Vector2(0, 0), new Vector2(1, 0), new Vector2(2, 0) },
				{new Vector2(0, 0), new Vector2(0, -1), new Vector2(1, -1), new Vector2(2,-1) },
				{new Vector2(-1, 1), new Vector2(-1, 0), new Vector2(0, 0), new Vector2(1, 0) },
				{new Vector2(0, 2), new Vector2(0, 1), new Vector2(1,1), new Vector2(2,1) },
			},
			{
				{new Vector2(2, 1), new Vector2(1, 1), new Vector2(1, 0), new Vector2(1,-1) },
				{new Vector2(1, 1), new Vector2(0, 1), new Vector2(0, 0), new Vector2(0,-1)},
				{new Vector2(0, 1), new Vector2(-1, 1), new Vector2(-1, 0), new Vector2(-1, -1)},
				{new Vector2(2, 2), new Vector2(1, 2), new Vector2(1, 1), new Vector2(1,0)}
			},
			{
				{new Vector2(0, 0), new Vector2(1, 0), new Vector2(2, 0), new Vector2(2, -1) },
				{new Vector2(-1, 0), new Vector2(0, 0), new Vector2(1,0), new Vector2(1, -1)},
				{new Vector2(0, 1), new Vector2(1, 1), new Vector2(2, 1), new Vector2(2, 0)},
				{new Vector2(0, 2), new Vector2(1, 2), new Vector2(2, 2), new Vector2(2, 1)}
			},
			{
				{new Vector2(1, 1), new Vector2(1, 0), new Vector2(1,-1), new Vector2(0,-1) },
				{new Vector2(2, 1), new Vector2(2, 0), new Vector2(2,-1), new Vector2(1, -1)},
				{new Vector2(0, 1), new Vector2(0, 0), new Vector2(0,-1), new Vector2(-1, -1)},
				{new Vector2(1, 2), new Vector2(1, 1), new Vector2(1, 0), new Vector2(0,0)}
			}
		},


		// SECOND IS L
		{
			{
				{new Vector2(0, 0), new Vector2(1, 0), new Vector2(2, 0), new Vector2(2, 1)},
				{new Vector2(-1, 0), new Vector2(0, 0), new Vector2(1, 0), new Vector2(1, 1) },
				{new Vector2(0, -1), new Vector2(1, -1), new Vector2(2, -1), new Vector2(2, 0) },
				{new Vector2(0, 1), new Vector2(1, 1), new Vector2(2, 1), new Vector2(2,2) },
			},
			{
				{new Vector2(1, 1), new Vector2(1, 0), new Vector2(1,-1), new Vector2(2,-1)},
				{new Vector2(0, 1), new Vector2(0, 0), new Vector2(0,-1), new Vector2(1,-1)},
				{new Vector2(-1, 1), new Vector2(-1, 0), new Vector2(-1, -1), new Vector2(0, -1)},
				{new Vector2(1, 2), new Vector2(1, 1), new Vector2(1, 0), new Vector2(2,0)}
			},
			{
				{new Vector2(0, -1), new Vector2(0, 0), new Vector2(1, 0), new Vector2(2, 0)},
				{new Vector2(-1,-1), new Vector2(-1, 0), new Vector2(0,0), new Vector2(1, 0)},
				{new Vector2(0, 0), new Vector2(0, 1), new Vector2(1, 1), new Vector2(2, 1)},
				{new Vector2(0, 1), new Vector2(0, 2), new Vector2(1, 2), new Vector2(2,2)}
			},
			{
				{new Vector2(0, 1), new Vector2(1, 1), new Vector2(1, 0), new Vector2(1,-1)},
				{new Vector2(1, 1), new Vector2(2, 1), new Vector2(2, 0), new Vector2(2,-1)},
				{new Vector2(-1, 1), new Vector2(0, 1), new Vector2(0, 0), new Vector2(0, -1)},
				{new Vector2(0, 2), new Vector2(1, 2), new Vector2(1, 1), new Vector2(1,0)}
			}
		},


		// LAST IS I
		{
			{
				{new Vector2(-1, 1), new Vector2(0, 1), new Vector2(1, 1), new Vector2(2, 1)},
				{new Vector2(-1, 0), new Vector2(0, 0), new Vector2(1, 0), new Vector2(2,0) },
				{new Vector2(-1, -1), new Vector2(0, -1), new Vector2(1, -1), new Vector2(2, -1) },
				{new Vector2(-1, 2), new Vector2(0, 2), new Vector2(1,2), new Vector2(2,2) },
			},
			{
				{new Vector2(1, 2), new Vector2(1, 1), new Vector2(1, 0), new Vector2(1, -1)},
				{new Vector2(0, 2), new Vector2(0, 1), new Vector2(0, 0), new Vector2(0, -1)},
				{new Vector2(-1, 2), new Vector2(-1, 1), new Vector2(-1, 0), new Vector2(-1, -1)},
				{new Vector2(2, 2), new Vector2(2, 1), new Vector2(2, 0), new Vector2(2, -1)}
			},
			{
				{new Vector2(-1, 0), new Vector2(0, 0), new Vector2(1, 0), new Vector2(2, 0)},
				{new Vector2(-1, -1), new Vector2(0, -1), new Vector2(1,-1), new Vector2(2, -1)},
				{new Vector2(-1, 1), new Vector2(0, 1), new Vector2(1, 1), new Vector2(2, 1)},
				{new Vector2(-1, 2), new Vector2(0, 2), new Vector2(1, 2), new Vector2(2,2)}
			},
			{
				{new Vector2(0, 2), new Vector2(0, 1), new Vector2(0, 0), new Vector2(0, -1)},
				{new Vector2(-1, 2), new Vector2(-1, 1), new Vector2(-1,0), new Vector2(-1, -1)},
				{new Vector2(1, 2), new Vector2(1, 1), new Vector2(1, 0), new Vector2(1, -1)},
				{new Vector2(2, 2), new Vector2(2, 1), new Vector2(2, 0), new Vector2(2,-1)}
			}
		}
	};


    List<int> SupperRotationShapeIndex;
    #endregion
    #region NestedTypes
    public struct CFieldSize {
        public int Left, Right, Bottom, Top,NewTop;
    }
    /// <summary>
    /// <list type="">
    ///     <item>Falling</item>
    ///     <item>Landed </item>
    ///     <item>Fixed< /item>
    /// </list>
    /// </summary>
    public enum TetrimoState {
        Spawning,   // Tetrimo is about to become "Falling"
        Falling,
        Landed,
        Fixed,
        Preview,
		Preview1,
		Preview2
    }
    #endregion

    #region Class instances data members
    /// <summary>All tetrimos in Preview State</summary>
	public static Tetrimo NextTetrimo;
	public static Tetrimo Next1Tetrimo;
	public static Tetrimo Next2Tetrimo;
    public static uint TetrimoCount = 1;
    /// <summary>border position of the game field (inclusive values)</summary>
    static CFieldSize FieldSize = new CFieldSize() {
        Left    =   0,
        Right   =  19,
        Top     =  9,  // always start at top-1, cause the center of each tetrimo is the second element on the lower row
		NewTop  = 11, // start on NewTop
        Bottom  =   0
    };
    /// <summary>Start position of new Tetrimos in 'Preview' state</summary>
    static Vector2 PreviewHUD = new Vector2() {
		x = 22.8f,
		y = 6.43f
    };
	static Vector2 Preview1HUD = new Vector2() {
		x = 22.8f,
		y = 2.5f
	};    
	static Vector2 Preview2HUD = new Vector2() {
		x = 22.8f,
		y = -1.2f
	};

    /// <summary>
    /// <list type="">
    ///     <item>null,         if position is free</item>
    ///     <item>gameObject,   if position is occupied </item>
    /// </list>
    /// </summary>
    static GameObject[,] FieldMatrix = new GameObject[10, 20];
	static GameObject[,] BottomLeftFieldMatrix = new GameObject[10, 10];
	static GameObject[,] BottomRightFieldMatrix = new GameObject[10, 10];
	static GameObject[,] TempFieldMatrix = new GameObject[10, 10];
    static Transform fallingTetrimo;
    #endregion

    #region Fields (Set in Unity Editor)
    /// <summary>Pointer to itselfs => create new Tetrimos</summary>
    public GameObject   TetrimoPrefab;
	public GameObject   TetrimoPartPrefab;
    public GameObject   TetrimoPartPreb_Ghost;
    public GameObject   TetrimoPartPreb_Black;

    public GameObject   TetrimoPartPrefab_i;
	public GameObject   TetrimoPartPrefab_j;
	public GameObject   TetrimoPartPrefab_l;
	public GameObject   TetrimoPartPrefab_o;
	public GameObject   TetrimoPartPrefab_s;
	public GameObject   TetrimoPartPrefab_t;
	public GameObject   TetrimoPartPrefab_z;
	public GameObject   TetrimoPartPrefab_rainbow;

    public GameObject   TetrimoExplosionPrefab;
    public GameObject   FourLinesLabelPrefab;

    // Effect 1 : Pha huy 1-3 chuoi lien ket
    // Effect 2 : Pha huy 4-6 chuoi lien ket
    // Effect 3 : Pha huy tren 7 chuoi lien ket
    public GameObject explisionEffect1;
    public GameObject explisionEffect2;
    public GameObject explisionEffect3;

    public GameObject hold;
	public GameObject board;
	public GameObject matrix10x20;
	public GameObject matrixBottomLeft10x10;
	public GameObject matrixBottomRight10x10;

    public TetrimoState State;
    /// <summary>Swap action cooldown</summary>
    public float        SwapCooldown;
    /// <summary>Horizontal movement speed</summary>
    public float        HorizontalSpeed;
    public float        FallingCooldown;
	float        FallingSpeed = 2f;

	public GameObject centerPos;
	public GameObject leftPos;
	public GameObject rightPos;
	public GameObject bottomPos;

	public GameObject blackHoleAnimation;

	Vector3 vectorDown;
	Vector3 vectorLeft;
	Vector3 vectorRight;
	Vector3 vectorUp;

	public List<int> listBlockFixed = new List<int>();

    

    #endregion

    #region Private Fields
    /// <summary>Countdown for next swap</summary>
    float NextSwap = 0.0f;
    /// <summary>Countdown for next fall</summary>
    float NextFall;
	int   ShapeIndex = -1;
	int RotationIndex;
    bool  IsMovingHorizontal = false;
	int moveScore = 0;
	bool isForceTurnLeft = false;
	bool isForceTurnRight = false;
	bool isForceTurn180degress = false;
    #endregion

    #region Check Can Move Or Rotate
    bool CanMoveDown {
        get {
            if(IsMovingHorizontal)
                return false;

            foreach (Transform child in transform) {
				if(Mathf.RoundToInt(child.position.y) > FieldSize.Top)
					return true;
                if (Mathf.RoundToInt(child.position.y - 1) < 0 || FieldMatrix[Mathf.RoundToInt(child.position.y - 1), Mathf.RoundToInt(child.position.x)] != null)
                    return false;
            }
            return true;
        }
    }
    bool CanMoveRight {
        get {
            bool canMoveRight = true;
            foreach (Transform child in transform) {
				if(Mathf.RoundToInt(child.position.y) > FieldSize.Top)
					return false;
                canMoveRight &= Mathf.RoundToInt(child.position.x + 1) <= Tetrimo.FieldSize.Right && FieldMatrix[Mathf.RoundToInt(child.position.y), Mathf.RoundToInt(child.position.x + 1)] == null;
            }
            return canMoveRight;
        }
    }
    bool CanMoveLeft {
        get {
            bool canMoveLeft = true;
            foreach (Transform child in transform) {
				if(Mathf.RoundToInt(child.position.y) > FieldSize.Top)
					return false;
                canMoveLeft &= Mathf.RoundToInt(child.position.x - 1) >= Tetrimo.FieldSize.Left && FieldMatrix[Mathf.RoundToInt(child.position.y), Mathf.RoundToInt(child.position.x - 1)] == null;
            }
            return canMoveLeft;
        }
    }

    bool CanRotate(Vector2 curPosVector2, string type = "right")
    {
        int tempRotationIndex = GetNextRotationIndex(type);

        for (int index = 0; index < Shapes.GetLength(2); index++)
        {
            Vector2 tmp = curPosVector2 + Shapes[ShapeIndex, tempRotationIndex, index];

            if (tmp.x < FieldSize.Left || tmp.x > FieldSize.Right || tmp.y < FieldSize.Bottom) // || tmp.y > FieldSize.Top)
                return false;

            if (tmp.y <= FieldSize.Top && FieldMatrix[Mathf.RoundToInt(tmp.y), Mathf.RoundToInt(tmp.x)] != null)
                return false;
        }
        return true;
    }
    #endregion

    //編集後
    Canvas thisCanvas;

    #region Event Handler
    void Start () {
		vectorUp = (centerPos.transform.position - bottomPos.transform.position);
		vectorDown = (bottomPos.transform.position - centerPos.transform.position);
		vectorLeft = (leftPos.transform.position - centerPos.transform.position);
		vectorRight = (rightPos.transform.position - centerPos.transform.position);

        //編集後
        thisCanvas = GameObject.Find("Canvas").GetComponent<Canvas>();

		RegisterEvent (true);
		moveScore = 0;
        SupperRotationShapeIndex = new List<int>(new int[2] { 0, 4 });

        switch (State) {
        case TetrimoState.Spawning:
            // For Debug
            DisplayMatrixField();
            GameController.Instance.CountDownTimeToRotation();
            NextFall = FallingCooldown;

			// If first spawning
			if(ShapeIndex == -1)
				ShapeIndex =  Random.Range(0, Shapes.GetLength(0));

            // Create shape and translate it at the center top of the game field.
            // REMARK: Do not use translate here! Instantiate create a copy of the current object.
            CreateShape();			
            transform.position = new Vector3((int)(FieldSize.Right / 2), FieldSize.NewTop - 1, 0);
            CreateAndMoveGhostTetris(true);
            // Check if the player has lost the game
            foreach (Transform child in transform) {
                if (FieldMatrix[(int)(child.position.y - 2), (int)child.position.x] != null) {
					GameController.Instance.SetGameEnd();
					break;
                }
            }
            name = "Tetrimo#" + (TetrimoCount-1);
            State = TetrimoState.Falling;
            fallingTetrimo = transform;
            break;

        case TetrimoState.Preview:
			Debug.Log ("PREVIEW : "+GameController.Instance.itemChangeType);
            
            if(ShapeIndex == -1)
           		ShapeIndex = Random.Range(0, Shapes.GetLength(0));
			switch(GameController.Instance.itemChangeType)
			{
				case "Change_i":
					ShapeIndex = 4;
					break;
				case "Change_j":
					ShapeIndex = 0;
					break;
				case "Change_l":
					ShapeIndex = 1;
					break;
				case "Change_s":
					ShapeIndex = 5;
					break;
				case "Change_z":
					ShapeIndex = 6;
					break;
				case "Change_t":
					ShapeIndex = 2;
					break;
				case "Change_o":
					ShapeIndex = 3;
					break;
			}
            CreateShape();
            transform.position = new Vector3(PreviewHUD.x, PreviewHUD.y);
			if(ShapeIndex == 4)
				transform.position = new Vector3(PreviewHUD.x + 0.2f, PreviewHUD.y);
			transform.localScale = new Vector3(0.7f,0.7f,0.7f);

            NextTetrimo = this;

            TetrimoCount++;
            name = "Tetrimo#" + TetrimoCount;
            break;

		case TetrimoState.Preview1:

			if(ShapeIndex == -1)
				ShapeIndex = Random.Range(0, Shapes.GetLength(0));
			CreateShape();
			transform.position = new Vector3(Preview1HUD.x, Preview1HUD.y);
			if(ShapeIndex == 4)
				transform.position = new Vector3(Preview1HUD.x + 0.2f, Preview1HUD.y);
			transform.localScale = new Vector3(0.7f,0.7f,0.7f);

			Next1Tetrimo = this;
			
			TetrimoCount++;
			name = "Tetrimo#" + TetrimoCount;
			break;
		case TetrimoState.Preview2:

			ShapeIndex = Random.Range(0, Shapes.GetLength(0));
			CreateShape();
			transform.position = new Vector3(Preview2HUD.x, Preview2HUD.y);
			if(ShapeIndex == 4)
				transform.position = new Vector3(Preview2HUD.x + 0.2f, Preview2HUD.y);
			transform.localScale = new Vector3(0.7f,0.7f,0.7f);

			Next2Tetrimo = this;
			
			TetrimoCount++;
			name = "Tetrimo#" + TetrimoCount;
			break;
        }

        
	}

    

    void Update() {
		if (GameController.Instance.isGamePause == true || GameController.Instance.isGameEnd)
			return;

		if (State != TetrimoState.Fixed && State != TetrimoState.Preview && State != TetrimoState.Preview1 && State != TetrimoState.Preview2 && State != TetrimoState.Spawning) {
            if (State != TetrimoState.Landed && Input.GetAxis("Vertical") < 0)

                StartCoroutine(FallingDown());

            if (Input.GetButtonDown("Horizontal"))
                StartCoroutine(MoveHorizontal());

            // Set "up" as alternative button for Jump (Project => Input)
            if ((Input.GetButton("Jump")) && Time.time > NextSwap) {
                OnRotateClick("right");
            }

            // Automatic falling down
            if (NextFall < 0) {
                StartCoroutine(FallingDown());
                NextFall = FallingCooldown;// ex
            }
            NextFall -= FallingSpeed * Time.deltaTime;
        }

        if(GameController.Instance.activeHoldButton && ghostTetrisParent != null && ghostTetrisParent.transform.childCount != transform.childCount)
        {
            GameController.Instance.activeHoldButton = false;
            CreateAndMoveGhostTetris();
        }

//        if (State == TetrimoState.Preview) {
//            transform.Rotate(0, 1f, 0, Space.World);
//        }

        if (Input.GetKeyDown("x"))
        {
            if (GameObject.Find("ChainEffect_test2") == null)
            {
                GameObject obj = Instantiate(Resources.Load("effect/ChainEffect_test2"), new Vector3(10, 0, -5), Quaternion.identity) as GameObject;
                obj.name = "ChainEffect_test2";
            }
        }
        if (Input.GetKeyDown("c"))
        {
            if (GameObject.Find("ChainEffect_test3") == null)
            {
                GameObject obj = Instantiate(Resources.Load("effect/ChainEffect_test3"), new Vector3(10, 0, -5), Quaternion.identity) as GameObject;
                obj.name = "ChainEffect_test3";
            }
        }
        if (Input.GetKeyDown("z"))
        {
            if (GameObject.Find("ChainEffect_test") == null)
            {
                GameObject obj = Instantiate(Resources.Load("effect/ChainEffect_test"), new Vector3(10, 0, -5), Quaternion.identity) as GameObject;
                obj.name = "ChainEffect_test";
            }
        }
    }
    #endregion

    #region Coroutines: http://unitygems.com/coroutines/

	void DisplayMatrixField()
	{
		if (matrix10x20 != null) 
		{
			for(int i=0; i<=FieldSize.Top; i++)
			{
				for(int j=0; j<=FieldSize.Right; j++)
				{
					string name = "FM"+i+j;
					GameObject obj = matrix10x20.transform.FindChild(name).gameObject;
					if(obj != null)
					{
						// If have tetris
						if(FieldMatrix[i,j] != null)
						{
							obj.GetComponent<Text>().text = "1";
						}
						else 
							obj.GetComponent<Text>().text = "0";
					}
					else 
						Debug.Log ("FM NULL in : "+i + j);
				}
			}
		}

		if (matrixBottomLeft10x10 != null) 
		{
			for(int i=0; i<FieldSize.Top; i++)
			{
				for(int j=0; j<FieldSize.Top; j++)
				{
					string name = "FM"+i+j;
					GameObject obj = matrixBottomLeft10x10.transform.FindChild(name).gameObject;
					if(obj != null)
					{
						// If have tetris
						if(BottomLeftFieldMatrix[j,i] != null)
							obj.GetComponent<Text>().text = "1";
						else 
							obj.GetComponent<Text>().text = "0";
					}
					else 
						Debug.Log ("FM LEFT NULL in : "+i + j);
				}
			}
		}

		if (matrixBottomRight10x10 != null) 
		{
			for(int i=0; i<FieldSize.Top; i++)
			{
				for(int j=0; j<FieldSize.Top; j++)
				{
					string name = "FM"+i+j;
					GameObject obj = matrixBottomRight10x10.transform.FindChild(name).gameObject;
					if(obj != null)
					{
						// If have tetris
						if(BottomRightFieldMatrix[j,i] != null)
							obj.GetComponent<Text>().text = "1";
						else 
							obj.GetComponent<Text>().text = "0";
					}
					else 
						Debug.Log ("FM RIGHT NULL in : "+i + j);
				}
			}
		}
	}

    /// <summary>
    /// Active the Tetrimo in the preview hub
    /// </summary>
    /// <returns></returns>
    IEnumerator ActivateTetrimoInPreview() {
        yield return 0;
    }
    /// <summary>
    /// Create a new Tetrimo and place it in the preview hub.
    /// </summary>
    /// <returns></returns>
    IEnumerator CreatePreviewStateTetrimo() {
        yield return 0;
    }
    IEnumerator MoveHorizontal(string type = "") {
        IsMovingHorizontal = true;

        float moved     = 0.0f;
        float direction = Input.GetAxis("Horizontal");
		if (string.IsNullOrEmpty (type) == false) {
			if(type == "right")
				direction = 1;
			else if(type == "left")
				direction = -1;
		}

        if ((this.CanMoveRight && direction > 0) || (this.CanMoveLeft && direction < 0)) {
            CheckShowHideGhostTetris();
            while (moved <= 1.0f) {
                float moveStep = Mathf.Min(HorizontalSpeed * Time.deltaTime, 1.1f - moved);   // 1.1f since float has some rounding problems!

                if (direction > 0)
                    transform.Translate(vectorRight * moveStep, Space.World);
                else if (direction < 0)
                    transform.Translate(vectorLeft  * moveStep, Space.World);

                moved += moveStep;
                yield return 0;
            } 

            // We will correct the actual position of each stone when it landed
            transform.position = new Vector2(Mathf.RoundToInt(transform.position.x), Mathf.RoundToInt(transform.position.y));
            MoveGhostTetris();
        }
        IsMovingHorizontal = false;
    }

    IEnumerator FallingDown()
    {
        //編集後
        if (this.CanMoveDown && !GameController.Instance.MultiCombo)
        {
            transform.Translate(vectorDown, Space.World);
			if(transform.childCount>0 && Mathf.RoundToInt(transform.GetChild(0).position.y) <= FieldSize.Top && FallingSpeed == 2f)
			{
				FallingSpeed = 1f;
			}
        }
        else {
            if (State == TetrimoState.Falling) {
                // After the Tetrimo has landed, the player can move it in the first 800 mili seconds.
                this.GetComponent<AudioSource>().Play();
                State = TetrimoState.Landed;

                
                yield return new WaitForSeconds(0.8f);

                while (IsMovingHorizontal)  // Wait for end of possible MoveHorizontal - calls
                    yield return new WaitForEndOfFrame();

                if (this.CanMoveDown) {
                    State = TetrimoState.Falling;
                }
                else {
                    DestroyGhostTetris();
                    fallingTetrimo = null;
                    State = TetrimoState.Fixed;
                    foreach (Transform child in transform) {
						if(Mathf.RoundToInt(child.position.y) <= FieldSize.Top)
                        	Tetrimo.FieldMatrix[Mathf.RoundToInt(child.position.y), Mathf.RoundToInt(child.position.x)] = child.gameObject;
                    }

					// Check can EAT or NOT
					StartCoroutine(EatAllLines());

					// Update MoveScore
					GameController.Instance.UpdateMoveScore(moveScore);
                }
            }
        }
    }

	bool isDoAfter = false;

	void DoAfterCheckEat()
	{
		isDoAfter = true;
		Debug.Log ("DoAfterCheckEat");
        
        GameController.Instance.CheckMakeRainbow();
		// Extract children (TetrimoParts) from Tetrimo. We will delete the Tetrimo itself.
		Transform[] children = GetComponentsInChildren<Transform>();
		for (int i = 0; i < children.Length; i++)
		{
			children[i].parent = null;
			children[i].parent = GameController.Instance.gameObject.transform;
		}

		if (isForceTurnLeft == true) 
		{
			GameController.Instance.DoBoardRotation(false, (bool isRightRotation) => {
				GameController.Instance.CountDownTimeToRotation(true);
				ChangeChildrenInFieldMatrixToLeft();
				ActivateAndCreateNewPreview();
				Destroy(gameObject);
			});
		} 
		else if (isForceTurnRight == true) 
		{
			GameController.Instance.DoBoardRotation(true, (bool isRightRotation) => {
				GameController.Instance.CountDownTimeToRotation(true);
				ChangeChildrenInFieldMatrix();
                ActivateAndCreateNewPreview();
				Destroy(gameObject);
			});
		}
		else if (isForceTurn180degress == true) 
		{
			GameController.Instance.DoBoardRotation(false, (bool isRightRotation) => {
				ChangeChildrenInFieldMatrixToLeft();
				GameController.Instance.DoBoardRotation(false, (bool isRightRotation1) => {
					GameController.Instance.CountDownTimeToRotation(true);
					ChangeChildrenInFieldMatrixToLeft();
                    ActivateAndCreateNewPreview();
					Destroy(gameObject);
				});
			});
		}
        else if(GameController.Instance.timeToRotate == 0)
        {
            GameController.Instance.DoBoardRotation(true, (bool isRightRotation) => {
                GameController.Instance.CountDownTimeToRotation(true);
                ChangeChildrenInFieldMatrix();
                ActivateAndCreateNewPreview();
                Destroy(gameObject);
            });
        }
        else
        {
            ActivateAndCreateNewPreview();
            Destroy(gameObject);
        }

        
	}

	void ActivateHoldSpawningMode()
	{
		// Reset speed to 2f;
		FallingSpeed = 2f;
		GameObject newGameObject = (GameObject)Instantiate(TetrimoPrefab, Vector3.zero, Quaternion.identity);
		Tetrimo newFallingTetrimo       = newGameObject.GetComponent<Tetrimo>();
		newFallingTetrimo.RotationIndex = RotationIndex;
		newFallingTetrimo.ShapeIndex    = GameController.Instance.holdShapeIndex;
		newFallingTetrimo.State         = TetrimoState.Spawning;
		
		foreach (Transform child in newFallingTetrimo.transform) {
			Destroy(child.gameObject);
		}
	}

    void ActivateAndCreateNewPreview() {
		if (GameController.Instance.holdCountDown > 0)
			GameController.Instance.holdCountDown--;

        if (GameController.Instance.holdCountDown == 0)
            GameController.Instance.activeHoldButton = false;

		GameController.Instance.itemChangeType = "";

        //編集後
        if (GameController.Instance.Combo >= 5 && GameController.Instance.Combo <= 6)
        {
            GamePlayUIManager ui = GameObject.Find("UIManager").GetComponent<GamePlayUIManager>();
            ui.ShowChainsText(GameController.Instance.Combo);
        }
        if (GameController.Instance.Combo >= 7)
        {
            GamePlayUIManager ui = GameObject.Find("UIManager").GetComponent<GamePlayUIManager>();
            ui.ShowChainsText(GameController.Instance.Combo);
        }
        //編集後
        
		GameController.Instance.ResetCombo ();
		// Reset speed to 2f;
		FallingSpeed = 2f;
        GameObject newGameObject = (GameObject)Instantiate(TetrimoPrefab, Vector3.zero, Quaternion.identity);
        Tetrimo newFallingTetrimo       = newGameObject.GetComponent<Tetrimo>();
        newFallingTetrimo.RotationIndex = NextTetrimo.RotationIndex;
        newFallingTetrimo.ShapeIndex    = NextTetrimo.ShapeIndex;
        newFallingTetrimo.State         = TetrimoState.Spawning;

        foreach (Transform child in newFallingTetrimo.transform) {
            Destroy(child.gameObject);
        }
        Destroy(NextTetrimo.gameObject);

        newGameObject = (GameObject)Instantiate(TetrimoPrefab, Vector3.zero, Quaternion.identity);
        Tetrimo newPreviewTetrimo = newGameObject.GetComponent<Tetrimo>();
		newPreviewTetrimo.ShapeIndex = Next1Tetrimo.ShapeIndex;
        newPreviewTetrimo.State = TetrimoState.Preview;

		foreach (Transform child in newPreviewTetrimo.transform) {
			Destroy(child.gameObject);
		}
		Destroy(Next1Tetrimo.gameObject);
		// Do with Preview1 and Preview2

		newGameObject = (GameObject)Instantiate(TetrimoPrefab, Vector3.zero, Quaternion.identity);
		Tetrimo newPreview1Tetrimo = newGameObject.GetComponent<Tetrimo>();
		newPreview1Tetrimo.ShapeIndex = Next2Tetrimo.ShapeIndex;
		newPreview1Tetrimo.State = TetrimoState.Preview1;
		
		foreach (Transform child in newPreview1Tetrimo.transform) {
			Destroy(child.gameObject);
		}
		Destroy(Next2Tetrimo.gameObject);

		newGameObject = (GameObject)Instantiate(TetrimoPrefab, Vector3.zero, Quaternion.identity);
		Tetrimo newPreview2Tetrimo = newGameObject.GetComponent<Tetrimo>();
		newPreview2Tetrimo.State = TetrimoState.Preview2;
		
		foreach (Transform child in newPreview2Tetrimo.transform) {
			Destroy(child.gameObject);
		}
    }

   

	void ChangePreviewTetris(string type)
	{
		Debug.Log ("ChangePreviewTetris : "+type);
		GameController.Instance.itemChangeType = type;
		GameObject newGameObject = (GameObject)Instantiate(TetrimoPrefab, Vector3.zero, Quaternion.identity);
		Tetrimo newFallingTetrimo = newGameObject.GetComponent<Tetrimo>();
		foreach (Transform child in newFallingTetrimo.transform) {
			Destroy(child.gameObject);
		}
		Destroy(NextTetrimo.gameObject);
//		
//		newGameObject = (GameObject)Instantiate(TetrimoPrefab, Vector3.zero, Quaternion.identity);
//		Tetrimo newPreviewTetrimo = newGameObject.GetComponent<Tetrimo>();
//		newPreviewTetrimo.State = TetrimoState.Preview;
	}

    IEnumerator RotateTetrimo(string type = "right") {
        this.GetComponent<AudioSource>().Play();
		foreach (Transform child in transform) {
			Destroy (child.gameObject);
		}

		RotationIndex = GetNextRotationIndex(type);
        CreateShape();
        yield return new WaitForEndOfFrame ();
        CreateAndMoveGhostTetris();
    }

	IEnumerator LetLinesAboveFallingVer3(int index, System.Action callback = null)
	{
        yield return new WaitForSeconds (1.5f);
		// TREN XUONG DUOI LEFT AND RIGHT
		for(int j = FieldSize.Top; j>0; j--)
		{
			for (int i = 1; i<=FieldSize.Top; i++) 
			{
				for(int k = 1; k<=FieldSize.Top; k++)
				{
					// LEFT
					if(FieldMatrix[k-1,10-j] == null)
					{
						FieldMatrix[k-1,10-j] = FieldMatrix[k,10-j];
						if(FieldMatrix[k-1,10-j] != null)
							FieldMatrix[k-1,10-j].transform.Translate(vectorDown,Space.World);
						FieldMatrix[k,10-j] = null;
					}

					// RIGHT
					if(FieldMatrix[k-1,9+j] == null)
					{
						FieldMatrix[k-1,9+j] = FieldMatrix[k,9+j];
						if(FieldMatrix[k-1, 9+j] != null)
							FieldMatrix[k-1, 9+j].transform.Translate(vectorDown,Space.World);
						FieldMatrix[k,9+j] = null;
					}
				}
			}
		}

		// BOTTOM
		for(int i = FieldSize.Top; i>0; i--)
		{
			for (int j=1; j<=FieldSize.Top; j++) 
			{
				for(int k=1; k<= FieldSize.Top; k++)
				{
					if(BottomLeftFieldMatrix[i-1,k-1] == null)
					{
						BottomLeftFieldMatrix[i-1,k-1] = BottomLeftFieldMatrix[i-1,k];
						if (BottomLeftFieldMatrix[i-1, k-1] != null)
						{
							BottomLeftFieldMatrix[i-1, k-1].transform.Translate(vectorUp,Space.World);
						}					
						BottomLeftFieldMatrix[i-1,k] = null;
					}
				}
			}
		}

		for(int i = FieldSize.Top; i>0; i--)
		{
			for (int j=1; j<=FieldSize.Top; j++) 
			{
				for(int k=1; k<= FieldSize.Top; k++)
				{
					if(BottomRightFieldMatrix[i-1,k-1] == null)
					{
						BottomRightFieldMatrix[i-1,k-1] = BottomRightFieldMatrix[i-1,k];
						if (BottomRightFieldMatrix[i-1, k-1] != null)
						{
							BottomRightFieldMatrix[i-1, k-1].transform.Translate(vectorUp,Space.World);
						}					
						BottomRightFieldMatrix[i-1,k] = null;
					}
				}
			}
		}

	    // TRAI PHAI PHIA TREN
		// TRAI SANG
		for(int i = FieldSize.Top; i>0; i--)
		{
			for (int j=FieldSize.Top-1; j>=0; j--) 
			{
				for(int k=FieldSize.Top-1; k>=0; k--)
				{
					if(FieldMatrix[i-1,k+1] == null)
					{
						FieldMatrix[i-1,k+1] = FieldMatrix[i-1,k];
						if(FieldMatrix[i-1,k+1] != null)
							FieldMatrix[i-1,k+1].transform.Translate(vectorRight,Space.World);
						FieldMatrix[i-1,k] = null;
					}
				}
			}
		}

		// PHAI SANG
		for(int i = FieldSize.Top; i>0; i--)
		{
			for (int j=FieldSize.Top+2; j<=FieldSize.Right; j++) 
			{
				for(int k=FieldSize.Top+2; k<=FieldSize.Right; k++)
				{
					if(FieldMatrix[i-1,k-1] == null)
					{
						FieldMatrix[i-1,k-1] = FieldMatrix[i-1,k];
						if(FieldMatrix[i-1,k-1] != null)
							FieldMatrix[i-1,k-1].transform.Translate(vectorLeft,Space.World);
						FieldMatrix[i-1,k] = null;
					}
				}
			}
		}

		// BOTTOM
		for (int j=FieldSize.Top; j>0; j--) 
		{
			for (int i=1; i<=FieldSize.Top; i++) 
			{
				for(int k=1; k<FieldSize.Top; k++)
				{
					if(BottomLeftFieldMatrix[k-1,j-1] == null)
					{
						BottomLeftFieldMatrix[k-1,j-1] = BottomLeftFieldMatrix[k,j-1];
						if (BottomLeftFieldMatrix[k-1, j-1] != null)
						{
							BottomLeftFieldMatrix[k-1, j-1].transform.Translate(vectorRight,Space.World);
						}					
						BottomLeftFieldMatrix[k,j-1] = null;
					}
				}
			}
		}

		for (int j=FieldSize.Top; j>0; j--) 
		{
			for (int i=1; i<=FieldSize.Top; i++) 
			{
				for(int k=1; k<FieldSize.Top; k++)
				{
					if(BottomRightFieldMatrix[k-1,j-1] == null)
					{
						BottomRightFieldMatrix[k-1,j-1] = BottomRightFieldMatrix[k,j-1];
						if (BottomRightFieldMatrix[k-1, j-1] != null)
						{
							BottomRightFieldMatrix[k-1, j-1].transform.Translate(vectorLeft,Space.World);
						}					
						BottomRightFieldMatrix[k,j-1] = null;
					}
				}
			}
		}

		if (callback != null)
        {
                yield return new WaitForSeconds(0.5f);
            
            callback();
        }
    }

    #endregion

    #region Helper functions
    void CreateShape() {
        gameObject.transform.localScale = new Vector3(1f, 1f, 1f);
        foreach (Transform child in transform) {
            Destroy(child.gameObject);
        }

        int rainbowIndex = -1;

        for (int i = 0; i < GameController.Instance.BlockHasRainbowLst.Count; i++)
        {
            if ((GameController.Instance.BlockHasRainbowLst[i] == 1 && State == TetrimoState.Preview) || (GameController.Instance.BlockHasRainbowLst[i] == 2 && State == TetrimoState.Preview1)
                || (GameController.Instance.BlockHasRainbowLst[i] == 3 && State == TetrimoState.Preview2)
                || (GameController.Instance.BlockHasRainbowLst[i] == 0 && State != TetrimoState.Preview && State != TetrimoState.Preview1 && State != TetrimoState.Preview2))
            {
                rainbowIndex = GameController.Instance.RainbowIndexLst[i];
            }
        }

        for (int index = 0; index < Shapes.GetLength(2); index++) {
			GameObject tmp = null;
            if (rainbowIndex >= 0 && index == rainbowIndex)
			{
				tmp = (GameObject)Instantiate(TetrimoPartPrefab_rainbow, Shapes[ShapeIndex, RotationIndex, index], Quaternion.identity);
			}
			else
			{
				switch(ShapeIndex)
				{
					case 0:
						tmp = (GameObject)Instantiate(TetrimoPartPrefab_j, Shapes[ShapeIndex, RotationIndex, index], Quaternion.identity);
						break;
					case 1:
						tmp = (GameObject)Instantiate(TetrimoPartPrefab_l, Shapes[ShapeIndex, RotationIndex, index], Quaternion.identity);
						break;
					case 2:
						tmp = (GameObject)Instantiate(TetrimoPartPrefab_t, Shapes[ShapeIndex, RotationIndex, index], Quaternion.identity);
						break;
					case 3:
						tmp = (GameObject)Instantiate(TetrimoPartPrefab_o, Shapes[ShapeIndex, RotationIndex, index], Quaternion.identity);
						break;
					case 4:
						tmp = (GameObject)Instantiate(TetrimoPartPrefab_i, Shapes[ShapeIndex, RotationIndex, index], Quaternion.identity);
						break;
					case 5:
						tmp = (GameObject)Instantiate(TetrimoPartPrefab_s, Shapes[ShapeIndex, RotationIndex, index], Quaternion.identity);
						break;
					case 6:
						tmp = (GameObject)Instantiate(TetrimoPartPrefab_z, Shapes[ShapeIndex, RotationIndex, index], Quaternion.identity);
						break;
					default:
						tmp = (GameObject)Instantiate(TetrimoPartPrefab_j, Shapes[ShapeIndex, RotationIndex, index], Quaternion.identity);
						break;

				}
			}
            tmp.transform.Translate(transform.position);
            tmp.transform.parent = gameObject.transform;
        }
    }

	IEnumerator EatAllLines()
	{
		int max = 20;
		List<int> indexToEatLst = new List<int> ();
		for (int i=0; i<max/2; i++) {
            // Do not check eat block at black holes
            if (i == 0 && GameController.Instance.IsBlackHoleShowed)
                continue;

			List<FieldMatrixStruct> checkTopLst = new List<FieldMatrixStruct>(FindTopFieldMatrix(i, max));
			List<FieldMatrixStruct> checkBottomLst = new List<FieldMatrixStruct>(FindBottomFieldMatrix(i));
			bool eatThisIndex = true;
			for(int j=0; j<checkTopLst.Count; j++)
			{
				if(FieldMatrix[checkTopLst[j].x, checkTopLst[j].y] == null)
				{
					eatThisIndex = false;
					break;
				}
			}

			if(eatThisIndex == true)
			{
				for(int j=0; j<checkBottomLst.Count; j++)
				{
					if(BottomLeftFieldMatrix[checkBottomLst[j].x, checkBottomLst[j].y] == null || 
					   BottomRightFieldMatrix[checkBottomLst[j].x, checkBottomLst[j].y] == null)
					{
						eatThisIndex = false;
						break;
					}
				}
			}

			if(eatThisIndex)
				indexToEatLst.Add(i);
		}

		// Thuc hien destroy tu vong ngoai vao trong
		for(int i=indexToEatLst.Count - 1; i >= 0; i--)
		{
			int totalEatBlock = 0;
			List<FieldMatrixStruct> checkTopLst = new List<FieldMatrixStruct>(FindTopFieldMatrix(indexToEatLst[i], max));
			List<FieldMatrixStruct> checkBottomLst = new List<FieldMatrixStruct>(FindBottomFieldMatrix(indexToEatLst[i]));
            float tempFadeDuration = 0.5f;
            for (int j = 0; j < checkTopLst.Count; j++)
            {
                StartCoroutine(_FadeObject(0, checkTopLst[j].x, checkTopLst[j].y, tempFadeDuration));
                totalEatBlock += 1;
            }

            for (int j = 0; j < checkBottomLst.Count; j++)
            {
                StartCoroutine(_FadeObject(1, checkBottomLst[j].x, checkBottomLst[j].y, tempFadeDuration));
                StartCoroutine(_FadeObject(2, checkBottomLst[j].x, checkBottomLst[j].y, tempFadeDuration));
                totalEatBlock += 2;
            }

            // Do move
            StartCoroutine(LetLinesAboveFallingVer3(indexToEatLst[i] + 1, () =>
            {
                StartCoroutine(EatAllLines());
            }));

            // Show Combo
            GameController.Instance.ShowCombo(indexToEatLst[i]);

            // Update Score
            GameController.Instance.UpdateScore(totalEatBlock);

            // Wait explosion effect finish
            yield return new WaitForSeconds(0.8f);
            GameController.Instance.ShowBlackHoleAnimation(indexToEatLst[i] + 1);

            // Chi thuc hien an vong ngoai cung
            break;
		}
            //編集後
            //連鎖が終了したらここを伸ばす

        float waittime = 0.1f;

        if (GameController.Instance.MultiCombo)
        {
            waittime += 1.0f;
            Debug.Log("wait");
        }
            yield return new WaitForSeconds(waittime);

        if (indexToEatLst.Count == 0 && isDoAfter == false)
        {
            DoAfterCheckEat();
        }
	}

    IEnumerator _FadeObject(int matrixIndex, int xIndex, int yIndex, float fadeDuration)
    {
        switch(matrixIndex)
        {
            case 0:
                CreateExplosionEffect(FieldMatrix[xIndex, yIndex].transform.position, Vector3.one);
                iTween.FadeTo(FieldMatrix[xIndex, yIndex], 0.5f, fadeDuration);
                yield return new WaitForSeconds(fadeDuration);
				CheckEatRainBowBlock(FieldMatrix[xIndex,yIndex].GetComponent<Renderer>().materials[0].name);
                Destroy(FieldMatrix[xIndex, yIndex]);
                FieldMatrix[xIndex, yIndex] = null;
                break;

            case 1:
                CreateExplosionEffect(BottomLeftFieldMatrix[xIndex, yIndex].transform.position, Vector3.one);
                iTween.FadeTo(BottomLeftFieldMatrix[xIndex, yIndex], 0.5f, fadeDuration);
                yield return new WaitForSeconds(fadeDuration);
			    CheckEatRainBowBlock(BottomLeftFieldMatrix[xIndex,yIndex].GetComponent<Renderer>().materials[0].name);
                Destroy(BottomLeftFieldMatrix[xIndex, yIndex]);
                BottomLeftFieldMatrix[xIndex, yIndex] = null;
                break;

            case 2:
                CreateExplosionEffect(BottomRightFieldMatrix[xIndex, yIndex].transform.position, Vector3.one);
                iTween.FadeTo(BottomRightFieldMatrix[xIndex, yIndex], 0.5f, fadeDuration);
                yield return new WaitForSeconds(fadeDuration);
			    CheckEatRainBowBlock(BottomRightFieldMatrix[xIndex,yIndex].GetComponent<Renderer>().materials[0].name);
                Destroy(BottomRightFieldMatrix[xIndex, yIndex]);
                BottomRightFieldMatrix[xIndex, yIndex] = null;
                break;
        }
        
        yield return null;
    }

	void CheckEatRainBowBlock(string name)
	{
		if (name.Contains("TetrimoPart_rainbow") == true)
		{
			// Add 1 GPoint when eat rainbow
			PlayerDataManager.Instance.ChangeGPoint(1);
			GameController.Instance.uiManager.ResetGpointInGamePlay();
		}
	}

    void CreateExplosionEffect(Vector3 positionVect, Vector3 scaleVect)
    {
        /*GameObject obj = explisionEffect1;
        if (GameController.Instance.Combo > 1)
            obj = explisionEffect2;
        if (GameController.Instance.Combo > 2)
            obj = explisionEffect3;*/

        //positionVect = new Vector3(positionVect.x, positionVect.y, -1);
        
        //GameObject createdObj = (GameObject)Instantiate(obj, positionVect, Quaternion.identity);
        //createdObj.name = "exp_" + GameController.Instance.Combo;
        //編集前
        //createdObj.transform.localScale = scaleVect;
        //編集後

        if (GameController.Instance.Combo >= 5 && GameController.Instance.Combo <= 6 || Input.GetKeyDown("z"))
        {
            if (GameObject.Find("ChainEffect_test2") == null)
            {
                GameObject obj = Instantiate(Resources.Load("effect/ChainEffect_test2"), new Vector3(10, 0, -5), Quaternion.identity) as GameObject;
                obj.name = "ChainEffect_test2";
            }
        }
        if (GameController.Instance.Combo > 6 || Input.GetKeyDown("x"))
        {
            if (GameObject.Find("ChainEffect_test3") == null)
            {
                GameObject obj = Instantiate(Resources.Load("effect/ChainEffect_test3"), new Vector3(10, 0, -5), Quaternion.identity) as GameObject;
                obj.name = "ChainEffect_test3";
                GameObject obj2 = Instantiate(Resources.Load("effect/ChainEffect_test2"), new Vector3(10, 0, -5), Quaternion.identity) as GameObject;
                obj2.name = "ChainEffect_test2";
            }
        }
        else
        {
            if (GameObject.Find("ChainEffect_test") == null)
            {
                GameObject obj = Instantiate(Resources.Load("effect/ChainEffect_test"), new Vector3(10, 0, -5), Quaternion.identity) as GameObject;
                obj.name = "ChainEffect_test";
            }
        }
    }

	List<FieldMatrixStruct> FindTopFieldMatrix(int index, int max)
	{
		List<FieldMatrixStruct> result = new List<FieldMatrixStruct> ();
		for (int i=0; i<index+1; i++) {
			FieldMatrixStruct f = new FieldMatrixStruct();
			f.x = i;
			f.y = max/2-index-1;
			result.Add(f);
			
			f = new FieldMatrixStruct();
			f.x = i;
			f.y = max/2+index;
			result.Add(f);
		}
		
		for(int j=max/2-index; j<max/2+index; j++) {
			FieldMatrixStruct f = new FieldMatrixStruct();
			f.x = index;
			f.y = j;
			result.Add(f);
		}
		return result;
	}
	
	List<FieldMatrixStruct> FindBottomFieldMatrix(int index)
	{
		List<FieldMatrixStruct> result = new List<FieldMatrixStruct> ();
		for (int i=0; i<index+1; i++) {
			FieldMatrixStruct f = new FieldMatrixStruct();
			f.x = i;
			f.y = index;
			result.Add(f);
			
			if(i != index)
			{
				f = new FieldMatrixStruct();
				f.x = index;
				f.y = i;
				result.Add(f);
			}
		}
		return result;
	}

    #endregion

	void ChangeChildrenInFieldMatrix()
	{
		// Buoc 1 : Luu BottomRightFieldMatrix sang 1 bien Temp;
		GameObject[,] Temp = new GameObject[10,10];
		for (int i=0;i<10; i++) 
		{
			for(int j=0;j<10;j++)
			{
				Temp[i,j] = BottomRightFieldMatrix[i,j];
			} 
		}

		// Buoc 2 : Cho FieldMatrix ben Phai xuong BottomRightFieldMatrix
		for (int i=0;i<10; i++) 
		{
			for(int j=0;j<10;j++)
			{
				BottomRightFieldMatrix[i,j] = FieldMatrix[i,j+10];
			} 
		}

		// Buoc 3 : Cho FieldMatrix ben Trai sang Phai
		for (int i=0; i<10; i++) 
		{
			for(int j=0; j<10;j++)
			{
				FieldMatrix[9-j,10+i] = FieldMatrix[i,j];
			}
		}

		// Buoc 4 : Cho BottomLeftFieldMatrix len FieldMatrix ben Trai 
		for (int i=0;i<10; i++) 
		{
			for(int j=0;j<10;j++)
			{
				FieldMatrix[i,9-j] = BottomLeftFieldMatrix[i,j];
			}
		}

		// Buoc 5 : Cho Temp vao BottomLeftFieldMatrix
		for (int i=0; i<10; i++) 
		{
			for(int j=0; j<10;j++)
			{
				BottomLeftFieldMatrix[j,i] = Temp[i,j];
			}
		}
	}

	void ChangeChildrenInFieldMatrixToLeft()
	{
		// Buoc 1 : Luu BottomRightFieldMatrix sang 1 bien Temp;
		GameObject[,] Temp = new GameObject[10,10];
		for (int i=0;i<10; i++) 
		{
			for(int j=0;j<10;j++)
			{
				Temp[i,j] = BottomLeftFieldMatrix[i,j];
			} 
		}
		
		// Buoc 2 : Cho FieldMatrix ben Trai xuong BottomLeftFieldMatrix
		for (int i=0;i<10; i++) 
		{
			for(int j=0;j<10;j++)
			{
				BottomLeftFieldMatrix[i,j] = FieldMatrix[i,9-j];
			}
		}
		
		// Buoc 3 : Cho FieldMatrix ben Phai sang Trai
		for (int i=0; i<10; i++) 
		{
			for(int j=0; j<10;j++)
			{
				FieldMatrix[i,j] = FieldMatrix[9-j,10+i];
			}
		}
		
		// Buoc 4 : Cho  BottomRightFieldMatrix len FieldMatrix ben Phai
		for (int i=0;i<10; i++) 
		{
			for(int j=0;j<10;j++)
			{
				FieldMatrix[i,j+10] = BottomRightFieldMatrix[i,j];
			} 
		}
		
		// Buoc 5 : Cho Temp vao BottomRightFieldMatrix
		for (int i=0; i<10; i++) 
		{
			for(int j=0; j<10;j++)
			{
				BottomRightFieldMatrix[i,j] = Temp[j,i];
			}
		}
	}


	#region Event Handler from other scene
	void OnMoveClick(string type)
	{
        if (GameController.Instance.isGamePause)
            return;
		if (State != TetrimoState.Fixed && State != TetrimoState.Preview && State != TetrimoState.Preview1 && State != TetrimoState.Preview2 && State != TetrimoState.Spawning) {
			switch(type)
			{
				case "down":
					if(State != TetrimoState.Landed)
					{
                        // 繝ｻNh蘯･n button 荳?dﾆｰ盻嬖, m盻擁 l蘯ｧn drop 1 ﾃｴ c盻冢g 1 ﾄ訴盻ノ
                        moveScore += 1;
                        StartCoroutine(FallingDown());
					}
					break;
				case "up":
					if(State != TetrimoState.Landed)
					{
						FallingSpeed = 500f;
                        //繝ｻNh蘯･n button 荳?trﾃｪn, s盻?ﾃｴ cho ﾄ黛ｺｿn khi drop*2 ﾄ訴盻ノ
                        moveScore += Mathf.Max(0, (Mathf.RoundToInt(transform.position.y) - GetMaxIndexOfGhostTetris())) * 2;
                        StartCoroutine(FallingDown());
					}
					break;
				case "left":
				case "right":
					StartCoroutine(MoveHorizontal(type));
					break;
			}
		}
	}

	void OnRotateClick(string type)
	{
        if (GameController.Instance.isGamePause)
            return;
        if (State != TetrimoState.Fixed && State != TetrimoState.Preview && State != TetrimoState.Preview1 && State != TetrimoState.Preview2 && State != TetrimoState.Spawning) {
			if (Time.time > NextSwap) {
                if (CanRotate(new Vector2(transform.position.x, transform.position.y), type) || CheckCanDoSupperRotation(type))
                {
                    StartCoroutine(RotateTetrimo(type));
                    NextSwap = Time.time + SwapCooldown;
                }
            }
		}
	}

	void OnChangeTetrisClick(string type)
	{
		if (State == TetrimoState.Preview)
			ChangePreviewTetris (type);	
	}

	void OnHOLDTetrisClick(string type)
	{
		Debug.Log ("OnHOLDTetrisClick");
		if (State == TetrimoState.Falling && GameController.Instance.holdCountDown == 0) 
		{
			GameController.Instance.holdCountDown = 2;
            GameController.Instance.activeHoldButton = true;
			foreach (Transform childTransform in hold.transform) {
				Destroy(childTransform.gameObject);
			}
			gameObject.transform.parent = hold.transform;
			gameObject.transform.localScale = new Vector3(0.5f,0.5f,0.5f);
			gameObject.transform.localPosition = new Vector3(0,0,0);
			if(ShapeIndex == 4)
				gameObject.transform.localPosition = new Vector3(-0.25f,0,0);

			State = TetrimoState.Fixed;
            DestroyGhostTetris();
			if(GameController.Instance.holdShapeIndex == -1)
			{
				ActivateAndCreateNewPreview ();
			}
			else
			{
				ActivateHoldSpawningMode ();
			}
			GameController.Instance.holdShapeIndex = ShapeIndex;
//			ActivateAndCreateNewPreview ();
//			Destroy (gameObject);
		}
	}

	void OnTurnButtonClicked (string type)
	{
		if (State != TetrimoState.Fixed && State != TetrimoState.Preview && State != TetrimoState.Preview1 && State != TetrimoState.Preview2 && State != TetrimoState.Spawning) 
		{			
			Debug.Log ("OnTurnButtonClicked : "+type);
			switch(type)
			{
				case "90left":
					isForceTurnLeft = true;
					isForceTurnRight = false;
					isForceTurn180degress = false;
					break;					
				case "90right":
					isForceTurnRight = true;
					isForceTurnLeft = false;
					isForceTurn180degress = false;
					break;					
				case "180":
					isForceTurn180degress = true;
					isForceTurnLeft = false;
					isForceTurnRight = false;
					break;
			}
		}
	}

	void RegisterEvent(bool isRegister)
	{
		if (isRegister) {
			Messenger<string>.AddListener ("OnMoveClick", OnMoveClick);
			Messenger<string>.AddListener ("OnRotateClick", OnRotateClick);
			Messenger<string>.AddListener ("OnChangeTetrisClick", OnChangeTetrisClick);
			Messenger<string>.AddListener ("OnHOLDTetrisClick", OnHOLDTetrisClick);
			Messenger<string>.AddListener ("OnTurnButtonClicked", OnTurnButtonClicked);
		} else {
			Messenger<string>.RemoveListener ("OnMoveClick", OnMoveClick);
			Messenger<string>.RemoveListener ("OnRotateClick", OnRotateClick);
			Messenger<string>.RemoveListener ("OnChangeTetrisClick", OnChangeTetrisClick);
			Messenger<string>.RemoveListener ("OnHOLDTetrisClick", OnHOLDTetrisClick); 
			Messenger<string>.RemoveListener ("OnTurnButtonClicked", OnTurnButtonClicked); 
		}
	}

	void OnDestroy()
	{
		RegisterEvent (false);
	}
	#endregion

	#region Ghost Tetris
    GameObject ghostTetrisParent;

    void CreateAndMoveGhostTetris(bool isInit = false, int maxRow = 10)
    {
        if (GameSettings.isShowGhostTetris)
        {
            if (isInit)
            {
                ghostTetrisParent = new GameObject("GhostTetris#"+(TetrimoCount-1));
                Messenger.AddListener("UpdateGhostTetris", UpdateGhostTetris);
            }
            
            CreateGhostTetris();
            MoveGhostTetris(maxRow);
        }
    }

    void CreateGhostTetris()
    {
        if (ghostTetrisParent != null)
        {
            Transform ghostTetrisParentTransform = ghostTetrisParent.transform;
            foreach (Transform child in ghostTetrisParentTransform)
            {
                Destroy(child.gameObject);
            }

            for (int index = 0; index < transform.childCount; index++)
            {
                GameObject tmp = (GameObject)Instantiate(TetrimoPartPreb_Ghost, transform.GetChild(index).localPosition, Quaternion.identity);
                tmp.transform.Translate(ghostTetrisParentTransform.position);
                tmp.transform.parent = ghostTetrisParentTransform;
            }
        }
    }

    void CheckShowHideGhostTetris()
    {
        if (ghostTetrisParent != null)
        {
            bool isShow = Mathf.RoundToInt(ghostTetrisParent.transform.position.y - transform.position.y) != 0;
            ghostTetrisParent.SetActive(isShow);
        }
    }

    void DestroyGhostTetris()
    {
        if (ghostTetrisParent != null)
        {
            GameObject.Destroy(ghostTetrisParent);
            ghostTetrisParent = null;
            Messenger.RemoveListener("UpdateGhostTetris", UpdateGhostTetris);
        }
    }

    void MoveGhostTetris(int maxRow = 10)
    {
        if (ghostTetrisParent != null)
        {
			int maxIndex = GetMaxIndexOfGhostTetris();
            ghostTetrisParent.transform.position = new Vector3(transform.position.x, maxIndex, ghostTetrisParent.transform.position.z + 0.01f);
        }
    }

	int GetMaxIndexOfGhostTetris()
	{
		int maxRow = Mathf.RoundToInt(transform.position.y);
		List<int> checkXPosLst = new List<int>();
		Dictionary<int, int> checkYPosDict = new Dictionary<int, int>();
		foreach (Transform child in transform)
		{
			int roundX = Mathf.RoundToInt(child.position.x);
			int roundY = Mathf.RoundToInt(child.localPosition.y);
			
			if (checkXPosLst.Contains(roundX))
			{
				if (checkYPosDict[roundX] > roundY)
					checkYPosDict[roundX] = roundY;
			}
			else
			{
				checkXPosLst.Add(roundX);
				checkYPosDict[roundX] = roundY;
			}
		}
		
		int maxIndex = 0;
		for (int i = 0; i < checkXPosLst.Count; i++)
		{
			for (int j = maxRow - 1; j >= 0; j--)
			{
				if (FieldMatrix[j, checkXPosLst[i]] != null)
				{
					maxIndex = j + 1 - checkYPosDict[checkXPosLst[i]] > maxIndex ? (j + 1 - checkYPosDict[checkXPosLst[i]]) : maxIndex;
					break;
				}
				
				if (j == 0 && FieldMatrix[j, checkXPosLst[i]] == null)
				{
					maxIndex = j - checkYPosDict[checkXPosLst[i]] > maxIndex ? (j - checkYPosDict[checkXPosLst[i]]) : maxIndex;
				}
			}
		}

		return maxIndex;
	}
	#endregion

    public static void SetBlock22ToBlackHole(GameObject[] objs)
    {
        if (objs == null)
        {
            GameObject tempObj = new GameObject();
            FieldMatrix[0, 9] = tempObj;
            FieldMatrix[0, 10] = tempObj;
            BottomLeftFieldMatrix[0, 0] = tempObj;
            BottomRightFieldMatrix[0, 0] = tempObj;
        }
        else
        {
            FieldMatrix[0, 9] = objs[0];
            FieldMatrix[0, 10] = objs[1];
            BottomLeftFieldMatrix[0, 0] = objs[2];
            BottomRightFieldMatrix[0, 0] = objs[3];
        }
    }

    #region Helper methods for Item Import/Export Block
    public static bool isEmptyBlock(int xIndex, int yIndex)
    {
        if(xIndex >= 0)
            return FieldMatrix[xIndex, yIndex] == null;
        else
        {
            if(yIndex <= 9)
            {
                return BottomLeftFieldMatrix[9 - yIndex, -xIndex-1] == null;
            }
            else
            {
                return BottomRightFieldMatrix[yIndex - 10, -xIndex-1] == null;
            }
        }
    }

    public static void AddOrRemoveBlock(int xIndex, int yIndex, GameObject _addedObj)
    {
        if (xIndex >= 0)
        {
            FieldMatrix[xIndex, yIndex] = _addedObj;
        }
        else
        {
            if (yIndex <= 9)
            {
                BottomLeftFieldMatrix[9 - yIndex, -xIndex-1] = _addedObj;
            }
            else
            {
                BottomRightFieldMatrix[yIndex - 10, -xIndex-1] = _addedObj;
            }
        }
        Messenger.Broadcast("UpdateGhostTetris", MessengerMode.DONT_REQUIRE_LISTENER);
    }

    public static bool IsBlackHoleBlock(int xIndex, int yIndex)
    {
        return ((xIndex == 0 || xIndex == -1) && (yIndex == 9 || yIndex == 10));
    }

    public static bool IsTetrimoBlock(int xIndex, int yIndex)
    {
        if (fallingTetrimo == null)
            return false;
        else
        {
            for(int i=0; i<fallingTetrimo.childCount; i++)
            {
                if (Mathf.RoundToInt(fallingTetrimo.GetChild(i).position.x - yIndex) == 0 && Mathf.RoundToInt(fallingTetrimo.GetChild(i).position.y - xIndex) == 0)
                    return true;
            }
            return false;
        }
    }

    void UpdateGhostTetris()
    {
        MoveGhostTetris();
    }
    #endregion

    #region Supper Rotation
    bool CheckCanDoSupperRotation(string type)
    {
        if (SupperRotationShapeIndex.Contains(ShapeIndex) == false)
            return false;

        Vector3 curPos = transform.position;
        int[,] supperMoveVector = GetSupperRotationMoveVector(type);

        bool isRotated = false;
        for (int i = 0; i < supperMoveVector.GetLength(1); i++)
        {
            if (CanRotate(new Vector2(curPos.x + supperMoveVector[0,i], curPos.y + supperMoveVector[1, i]), type))
            {
                transform.position = new Vector3(curPos.x + supperMoveVector[0, i], curPos.y + supperMoveVector[1, i], curPos.z);
                isRotated = true;
                break;
            }
        }

        return isRotated;
    }

    int GetNextRotationIndex(string type = "right")
    {
        int tempRotationIndex = (RotationIndex + (type == "left" ? -1 : 1));
        if (tempRotationIndex < 0)
            tempRotationIndex = Shapes.GetLength(2) - 1;
        tempRotationIndex = tempRotationIndex % Shapes.GetLength(2);

        return tempRotationIndex;
    }

	int GetShapeIndexOfSupperBlock(int shapeIdx)
	{
		int temp = shapeIdx;
		if (temp == 4)
			temp = 2;
		return temp;
	}

    int[,] GetSupperRotationMoveVector(string type = "right")
    {
        int[,] result = new int[2, 1] { { 0 }, { 0 } };

        switch(ShapeIndex)
        {
            case 0:
                if (RotationIndex % 4 == 0)
                {
                    if (type.Equals("right"))
                        result = new int[2, 4] { { -1, -1, 0, -1 }, { 0, 1, -2, -2 } };
                    else
                        result = new int[2, 4] { { 1, 1, 0, 1 }, { 0, 1, -2, -2 } };
                }
                else if (RotationIndex % 4 == 1)
                {
                    if (type.Equals("right"))
                        result = new int[2, 4] { { 1, 1, 0, 1 }, { 0, -1, 2, 2 } };
                    else
                        result = new int[2, 4] { { 1, 1, 0, 1 }, { 0, -1, 2, 2 } };
                }
                else if (RotationIndex % 4 == 2)
                {
                    if (type.Equals("right"))
                        result = new int[2, 4] { { 1, 1, 0, 1 }, { 0, 1, -2, -2 } };
                    else
                        result = new int[2, 4] { { -1, -1, 0, -1 }, { 0, 1, -2, -2 } };
                }
                else if (RotationIndex % 4 == 3)
                {
                    if (type.Equals("right"))
                        result = new int[2, 4] { { -1, -1, 0, -1 }, { 0, -1, 2, 2 } };
                    else
                        result = new int[2, 4] { { -1, -1, 0, -1 }, { 0, -1, 2, 2 } };
                }
                break;

            case 4:
                if (RotationIndex % 4 == 0)
                {
                    if (type.Equals("right"))
                        result = new int[2, 4] { { -2, 1, 1, -2 }, { 0, 0, 2, -1 } };
                    else
                        result = new int[2, 4] { { -1, 2, -1, 2 }, { 0, 0, 2, -1 } };
                }
                else if (RotationIndex % 4 == 1)
                {
                    if (type.Equals("right"))
                        result = new int[2, 4] { { 2, -1, -1, 2 }, { 0, 0, 2, -1 } };
                    else
                        result = new int[2, 4] { { -2, 1, 1, -2 }, { 0, 0, 2, -1 } };
                }
                else if (RotationIndex % 4 == 2)
                {
                    if (type.Equals("right"))
                        result = new int[2, 4] { { 2, -1, 2, -1 }, { 0, 0, 1, -1 } };
                    else
                        result = new int[2, 4] { { -2, 1, -2, 1 }, { 0, 0, 1, -1 } };
                }
                else if (RotationIndex % 4 == 3)
                {
                    if (type.Equals("right"))
                        result = new int[2, 4] { { -1, 2, -1, 2 }, { 0, 0, 1, -2 } };
                    else
                        result = new int[2, 4] { { 2, -1, 2, -1 }, { 0, 0, 1, -2 } };
                }
                break;
        }

        return result;
    }

    #endregion

    
}
