using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SimpleLibs;
public class ShopManager : MonoBehaviour {
	public GameObject shop;
	public GameObject gpointObj;
	public AudioClip shopSceneMusic;

	// Use this for initialization
	void Start () {
//		if (shop != null) 
//		{
//			shop.transform.localScale = Vector3.zero;
//			iTween.ScaleTo(shop, Vector3.one, 1.5f);
//		}

        if(shopSceneMusic != null)
		    SoundManager.Instance.PlayMusic (shopSceneMusic, true);

        UpdateGPointUI();
        Messenger.AddListener("UpdateGPointUI", UpdateGPointUI);
	}

    void OnDestroy()
    {
        Messenger.RemoveListener("UpdateGPointUI", UpdateGPointUI);
    }

	void OnEnable()
	{
		if (shop != null)
		{
			shop.transform.localScale = Vector3.zero;
			iTween.ScaleTo(shop, Vector3.one, 1.5f);
		}
	}

	public void OnClickBackButton()
	{
		iTween.ScaleTo(shop, iTween.Hash("scale",Vector3.zero , "islocal", true, "time", 1f, "delay", 0f,
		                                            "oncompletetarget", this.gameObject, "oncomplete", "ReturnMainScene"));
	}

	public void OnClickBackButtonInGamePlay()
	{
		iTween.ScaleTo(shop, iTween.Hash("scale",Vector3.zero , "islocal", true, "time", 1f, "delay", 0f,
		                                 "oncompletetarget", this.gameObject, "oncomplete", "ReturnItem"));
	}

	public void ReturnItem()
	{
		this.gameObject.SetActive(false);
	}

	public void ReturnMainScene()
	{
		Application.LoadLevel("MainScene");
	}

	public void OnClickBuyButton(int index)
	{
		BuyIndex(index - 1);
	}

	void BuyIndex(int index)
	{
#if UNITY_ANDROID
		if(InAppPurchaseManager.Instance != null && index < Configuration.GOOGLE_SKUS.Length)
		{
			InAppPurchaseManager.Instance.BuyItem(Configuration.GOOGLE_SKUS[index]);
		}
        else
        {
            Debug.Log("Something wrong with configuration");
        }
#endif
	}

    void UpdateGPointUI()
    {
        gpointObj.GetComponent<Text>().text = "GPOINT : " + PlayerDataManager.Instance.GPoint;
    }

	// Update is called once per frame
	void Update () {
	
	}
}
