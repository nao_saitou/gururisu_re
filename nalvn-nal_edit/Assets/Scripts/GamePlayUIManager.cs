﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleLibs;
using DG.Tweening;
using UnityEngine.UI;
public class GamePlayUIManager : MonoBehaviour
{
    const long maxScoreToShow = 9999999999; // 10 so
    public GameObject itemPanel;
    public GameObject gpointInItem;
    public Text turnToRotate;
    public SceneController sceneController;

    public List<Sprite> listNumberImage;
    public List<Image> scoreImageLst;
    public GameObject Score;

    public List<Sprite> listComboNumberImage;

    //編集後
    public GameObject combo2;
    public Text ComboText;
    float ComboDeleteTime;
    public Text ChainsText;
    public Text ChainsNumberText;
    float ChainTextDeletetime;
    //

    public GameObject ComboObj;
    private Image ComboImage;
    public Image ComboNumber1;
    public Image ComboNumber2;
    public Text GPoint;
    public GameObject shopPanel;

    string turnToRotateFormat = "回転まであと{0}";
    string endGameResultFormat = "ゲームオーバーです\nスコア：{0}\nもう一度プレイしますか？";

    float timePressButton;
    bool isPressingButton;
    string pressButtonType;
    const float TIME_PRESS = 0.2f;

    string changeItemType = "";
    bool isGamePauseBeforeQuitConfirm = false;
    void Start()
    {
        sceneController.callback += ShowQuitGame;
        timePressButton = 0f;
        isPressingButton = false;
        pressButtonType = "";
        ResetGpointInGamePlay();
        Messenger.AddListener("UpdateGPointUI", ResetGpointInGamePlay);
    }

    void OnDestroy()
    {
        Messenger.RemoveListener("UpdateGPointUI", ResetGpointInGamePlay);
    }

    void Update()
    {
        if (isPressingButton && timePressButton > 0)
        {
            timePressButton -= Time.deltaTime;
            if (timePressButton <= 0)
            {
                OnMoveButtonTap(pressButtonType);
                timePressButton = TIME_PRESS;
            }
        }

        //編集後
        if (Input.GetKeyDown("x"))
        {
        }

        if (ComboDeleteTime <= 0.0f)
        {
            ComboText.enabled = false;
        }

        if (ChainTextDeletetime <= 0.0f)
        {
            ChainsText.enabled = false;
            ChainsNumberText.enabled = false;
        }

        ChainTextDeletetime -= Time.deltaTime;
        ComboDeleteTime -= Time.deltaTime;
    }

    public void ResetGpointInGamePlay()
    {
        gpointInItem.GetComponent<Text>().text = "GPoint : " + PlayerDataManager.Instance.GPoint;
        GPoint.text = "GPoint : " + PlayerDataManager.Instance.GPoint;
    }

    public void ChangeTheScore(long score)
    {
        if (score > maxScoreToShow)
            score = maxScoreToShow;

        string scoreStr = score.ToString();
        for (int i = 0; i < scoreImageLst.Count; i++)
        {
            scoreImageLst[i].gameObject.SetActive(i < scoreStr.Length);
            if (scoreImageLst[i].gameObject.activeSelf)
                scoreImageLst[i].sprite = listNumberImage[int.Parse(scoreStr.Substring(i, 1))];
        }
    }

    public void ShowQuitGame()
    {
        sceneController.blockOnCallBack = true;
        sceneController.callback -= ShowQuitGame;
        isGamePauseBeforeQuitConfirm = GameController.Instance.isGamePause;
        GameController.Instance.isGamePause = true;
        AlertManager.Instance.ShowAlert("TOP画面に戻りますか？", "終了する", "終了しない", gameObject, "OnQuitGameYes", "OnQuitGameNo");
    }

    private void OnQuitGameYes()
    {
        Application.LoadLevel(sceneController.previousScene);
    }

    private void OnQuitGameNo()
    {
        sceneController.callback += ShowQuitGame;
        sceneController.blockOnCallBack = false;
        GameController.Instance.isGamePause = isGamePauseBeforeQuitConfirm;
    }

    public void ShowEndGame(long score = 0)
    {
        AlertManager.Instance.ShowAlert(string.Format(endGameResultFormat, score), "もう一度", "終了します", gameObject, "OnEndGameRetry", "OnEndGameQuit");
    }

    private void OnEndGameRetry()
    {
        GameController.Instance.StartNewGame();
    }

    private void OnEndGameQuit()
    {
        Application.LoadLevel("MainScene");
    }

    public void SetTurnToRotate(int countDown)
    {
        turnToRotate.text = string.Format(turnToRotateFormat, countDown);
        turnToRotate.color = countDown == 0 ? Color.red : Color.white;
    }

    #region Button Events
    public void OnMoveButtonTap(string type)
    {
        Messenger<string>.Broadcast("OnMoveClick", type, MessengerMode.DONT_REQUIRE_LISTENER);
    }

    public void OnRotateButtonTap(string type)
    {
        Messenger<string>.Broadcast("OnRotateClick", type, MessengerMode.DONT_REQUIRE_LISTENER);
    }

    public void OnHOLDButtonClick()
    {
        if (GameController.Instance.BlockInput == false)
        {
            Messenger<string>.Broadcast("OnHOLDTetrisClick", "", MessengerMode.DONT_REQUIRE_LISTENER);
        }
    }

    public void OnItemButtonClick()
    {
        if (GameController.Instance.BlockInput == false)
        {
            GameController.Instance.isGamePause = true;
            ResetGpointInGamePlay();
            //		itemPanel.SetActive (true);
            itemPanel.transform.DOLocalMoveX(-121f, 1f);
        }
    }

    public void OnBackButtonClick()
    {
        GameController.Instance.isGamePause = false;
        itemPanel.transform.DOLocalMoveX(-800f, 1f);
    }

    public void OnChangeTetrisButtonClick(string type)
    {
        if (PlayerDataManager.Instance.GPoint < Configuration.ITEM_CHANGE_BLOCK_COST)
        {
            AlertManager.Instance.ShowAlert("Gポイントが足りません、購入しますか？", "はい", "いいえ", gameObject, "OnGoToShopConfirmed", "");
        }
        else
        {
            changeItemType = type;
            string titleStr = string.Format("ＮＥＸＴブロックを「{0}」ブロックに変化させます。よろしいですか？\n", Utilities.GetChangeTetrisBlock(type)) + string.Format(Configuration.ITEM_COST_FORMAT, Configuration.ITEM_CHANGE_BLOCK_COST);
            AlertManager.Instance.ShowAlert(titleStr, "使用する", "使用しない", gameObject, "OnChangeTetrisConfirmed", "");
        }
    }

    public void OnTurnButtonClicked(string type)
    {
        if (PlayerDataManager.Instance.GPoint < Configuration.ITEM_ROTATE_BOARD_COST)
        {
            AlertManager.Instance.ShowAlert("Gポイントが足りません、購入しますか？", "はい", "いいえ", gameObject, "OnGoToShopConfirmed", "");
        }
        else
        {
            switch (type)
            {
                case "90left":
                    AlertManager.Instance.ShowAlert("ターン終了時に左に９０度回転させます。よろしいですか？\n" + string.Format(Configuration.ITEM_COST_FORMAT, Configuration.ITEM_ROTATE_BOARD_COST),
                    "使用する", "使用しない", gameObject, "On90LeftTurnConfirmed", "");
                    break;
                case "90right":
                    AlertManager.Instance.ShowAlert("ターン終了時に右に９０度回転させます。よろしいですか？\n" + string.Format(Configuration.ITEM_COST_FORMAT, Configuration.ITEM_ROTATE_BOARD_COST),
                    "使用する", "使用しない", gameObject, "On90RightTurnConfirmed", "");
                    break;
                case "180":
                    AlertManager.Instance.ShowAlert("ターン終了時に１８０度回転させます。よろしいですか？\n" + string.Format(Configuration.ITEM_COST_FORMAT, Configuration.ITEM_ROTATE_BOARD_COST),
                    "使用する", "使用しない", gameObject, "On180TurnConfirmed", "");
                    break;
            }
        }
    }

    public void OnButtonDown(string type)
    {
        // Debug.Log ("OnButtonDown : "+System.DateTime.Now.ToString());
        isPressingButton = true;
        timePressButton = TIME_PRESS;
        pressButtonType = type;
    }

    public void OnButtonUp(string type)
    {
        // Debug.Log ("OnButtonUP : "+System.DateTime.Now.ToString());
        isPressingButton = false;
        timePressButton = 0f;
        pressButtonType = "";
    }

    public void ShowCombo(int index, int combo)
    {
        //修正前
        /*// Make sure list number is setup correct
        if (listComboNumberImage.Count != 10)
            return;

        if (ComboObj.activeSelf)
            iTween.Stop(ComboObj);
        else
            ComboObj.SetActive(true);

        float curXPos = ComboObj.transform.localPosition.x;
        float startYPos = 20 * index - 1;
        float EndYPos = 20 * index;

        ComboObj.transform.localPosition = new Vector3(curXPos, startYPos, 0);

        ComboNumber2.gameObject.SetActive(combo > 9);
        if (combo >= 10)
        {
            int number1 = combo / 10;
            int number2 = combo % 10;

            ComboNumber1.sprite = listComboNumberImage[number1];
            ComboNumber2.sprite = listComboNumberImage[number2];
        }
        else
        {
            ComboNumber1.sprite = listComboNumberImage[combo];
        }

        iTween.MoveTo(ComboObj, iTween.Hash("position", new Vector3(curXPos, EndYPos, 0), "islocal", true, "time", 0.5f, "easetype", "linear", "oncomplete", "OnMoveComboObjFinished", "oncompletetarget", gameObject));
        iTween.ValueTo(ComboObj, iTween.Hash("from", 255, "to", 110, "time", 0.5f, "onupdatetarget", gameObject, "onupdate", "OnUpdateComboObj"));
         * */

        //編集後
        if (combo >= 2)
        {
            ComboText.enabled = true;
            ComboDeleteTime = 1f;
            ComboText.text = combo + "COMBO";
        }
    }

    public void ShowChainsText(int combo)
    {

        ChainsText.enabled = true;
        ChainsNumberText.enabled = true;

        if (combo >= 5 && combo <= 6)
        {
            ChainsText.text = "EXCELLENT!";
        }
        if (combo >= 7)
        {
            ChainsText.text = "CONGRATULATIONS!!";
        }

        ChainsNumberText.text = "MAX" + combo.ToString() + "CHAIN!";

        ChainTextDeletetime = 1.5f;
    }

    public void ImportExportButtonEvent(bool isImport)
    {
        if (PlayerDataManager.Instance.GPoint < Configuration.ITEM_IMPORT_EXPORT_COST)
        {
            AlertManager.Instance.ShowAlert("Gポイントが足りません、購入しますか？", "はい", "いいえ", gameObject, "OnGoToShopConfirmed", "");
        }
        else
        {
            if (isImport)
            {
                AlertManager.Instance.ShowAlert("選択するマスにブロックを置きます。よろしいですか？\n" + string.Format(Configuration.ITEM_COST_FORMAT, Configuration.ITEM_IMPORT_EXPORT_COST),
                    "使用する", "使用しない", gameObject, "OnImportBlockConfirmed", "");
            }
            else
            {
                AlertManager.Instance.ShowAlert("選択するマスにブロックを取り除きます。よろしいですか？\n" + string.Format(Configuration.ITEM_COST_FORMAT, Configuration.ITEM_IMPORT_EXPORT_COST),
                    "使用する", "使用しない", gameObject, "OnExportBlockConfirmed", "");
            }
        }
    }
    #endregion

    void OnMoveComboObjFinished()
    {
        ComboObj.SetActive(false);
    }

    void OnUpdateComboObj(float alphaColor)
    {
        if (ComboImage == null)
            ComboImage = ComboObj.GetComponent<Image>();

        alphaColor /= 255;
        ComboImage.color = new Color(ComboImage.color.r, ComboImage.color.g, ComboImage.color.b, alphaColor);
        ComboNumber1.color = new Color(ComboNumber1.color.r, ComboNumber1.color.g, ComboNumber1.color.b, alphaColor);
        ComboNumber2.color = new Color(ComboNumber2.color.r, ComboNumber2.color.g, ComboNumber2.color.b, alphaColor);
    }

    void OnImportBlockConfirmed()
    {
        PlayerDataManager.Instance.ChangeGPoint(-Configuration.ITEM_IMPORT_EXPORT_COST);
        ResetGpointInGamePlay();
        itemPanel.transform.DOLocalMoveX(-800f, 1f);
        GameController.Instance.AddImportExportBlock(true);
    }

    void OnExportBlockConfirmed()
    {
        PlayerDataManager.Instance.ChangeGPoint(-Configuration.ITEM_IMPORT_EXPORT_COST);
        ResetGpointInGamePlay();
        itemPanel.transform.DOLocalMoveX(-800f, 1f);
        GameController.Instance.AddImportExportBlock(false);
    }


    void On90LeftTurnConfirmed()
    {
        PlayerDataManager.Instance.ChangeGPoint(-Configuration.ITEM_ROTATE_BOARD_COST);
        ResetGpointInGamePlay();
        Messenger<string>.Broadcast("OnTurnButtonClicked", "90left", MessengerMode.DONT_REQUIRE_LISTENER);
        OnBackButtonClick();
    }
    void On90RightTurnConfirmed()
    {
        PlayerDataManager.Instance.ChangeGPoint(-Configuration.ITEM_ROTATE_BOARD_COST);
        ResetGpointInGamePlay();
        Messenger<string>.Broadcast("OnTurnButtonClicked", "90right", MessengerMode.DONT_REQUIRE_LISTENER);
        OnBackButtonClick();
    }
    void On180TurnConfirmed()
    {
        PlayerDataManager.Instance.ChangeGPoint(-Configuration.ITEM_ROTATE_BOARD_COST);
        ResetGpointInGamePlay();
        Messenger<string>.Broadcast("OnTurnButtonClicked", "180", MessengerMode.DONT_REQUIRE_LISTENER);
        OnBackButtonClick();
    }

    void OnChangeTetrisConfirmed()
    {
        PlayerDataManager.Instance.ChangeGPoint(-Configuration.ITEM_CHANGE_BLOCK_COST);
        ResetGpointInGamePlay();
        Messenger<string>.Broadcast("OnChangeTetrisClick", changeItemType, MessengerMode.DONT_REQUIRE_LISTENER);
    }

    void OnGoToShopConfirmed()
    {
        shopPanel.SetActive(true);
    }
}