﻿using UnityEngine;
using System.Collections;

public class Configuration{
	// When start the first time, player will receive 50 GPoints
    public const int START_GPOINT = 50;
    public const int ITEM_IMPORT_EXPORT_COST = 5;
    public const int ITEM_CHANGE_BLOCK_COST = 5;
    public const int ITEM_ROTATE_BOARD_COST = 5;
	public const int RAINBOW_COUNTDOWN = 5;

    public const string ITEM_COST_FORMAT = "GPoint {0}消費";

    // Google skus
	public static string[] GOOGLE_SKUS = new string[3] { "jp.icnet.gururisu.gpoint100", "jp.icnet.gururisu.gpoint500", "jp.icnet.gururisu.gpoint1000" };
    public static int[] GPOINT_ARRAY = new int[3] { 100, 500, 1000 };
	public const string IAB_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAxcGP1Q7J9qWDt/z9TA4cKZ55txlXIQVmkrFE8C9vbfOouVMQ4bB5kznRdFzu805d2zj9KWR8ai9iXjgrPQaYdzyudGVLhWpDfRM96Sb2YrZewbs85amT39KnMLkkz1TL9wa1WIMzTh8Y3llhIn29niwCs8PPdM5gGJcSrEr3cso58iHxPw5c0yNJlznf3cxyI5ozPgDUfreLgjxMAY/nFssFzeV3h9a2Y3BXCOwWPgar4VDQ6jli7fLOGaXTvVFZXLV//Sm83jCKjw91r9tPGfmjnX3ttXfPVwgdm1MlSzBkZAVipAib5dgw62FDfdUsQNS+ScVIKd7MkSBrmjieAQIDAQAB";

}
