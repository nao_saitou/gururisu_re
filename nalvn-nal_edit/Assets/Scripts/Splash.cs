﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

public class Splash : MonoBehaviour {
	public GameObject touchText;
	public Text versionText;
	public AudioClip splashMusic;
	bool isLoading = false;
	float changeTime = 0;
	void Start()
	{
		SoundManager.Instance.PlayMusic (splashMusic, true);
		versionText.text = "Ver. 1.0.0";
	}

	// Update is called once per frame
	void Update () {
		changeTime += Time.deltaTime;
		if (changeTime > 1f) {
			changeTime = 0;
			if (touchText.activeSelf) {
				touchText.SetActive (false);
			} else {
				touchText.SetActive (true);
			}

		}

#if UNITY_EDITOR
		if(Input.GetMouseButtonDown(0) == true)
		{
			GoToMainScene();
		}
#else
		if(Input.touchCount > 0)
		{
			Touch touch = Input.GetTouch(0);
			if(touch.phase == TouchPhase.Ended)
			{
				GoToMainScene();
			}
		}
#endif
	}

	void GoToMainScene()
	{
		if (isLoading == false) {
			isLoading = true;
			Application.LoadLevel("MainScene");
		}
	}
}
