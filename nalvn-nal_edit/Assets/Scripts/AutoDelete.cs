﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AutoDelete : MonoBehaviour
{
    [SerializeField]
    bool autoDelete;

    [SerializeField]
    bool autoEnable;

    [SerializeField]
    float DeleteTime;
	
	void Update ()
    {
        if (autoDelete)
        {
            if (DeleteTime <= 0.0f)
            {
                Destroy(this.gameObject);
            }
        }

        if (autoEnable)
        {
            if (DeleteTime <= 0.0f)
            {
                //GetComponent<Text>().enabled = false;
            }
        }

        DeleteTime -= Time.deltaTime;
	}
}
