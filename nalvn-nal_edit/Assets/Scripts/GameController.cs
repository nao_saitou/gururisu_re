﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
public class GameController : MonoBehaviour
{

    public long gameScore = 0;
    public int timeToRotate = 1;
    public string itemChangeType = "";
    public bool isGamePause = false;
    public bool isGameEnd = false;
    public AudioClip musicSource1;
    public AudioClip musicSource2;
    public GamePlayUIManager uiManager;
    public bool isShowGhostTetris = false;
    public int holdShapeIndex = -1;
    public int holdCountDown = 0;
    public bool activeHoldButton = false;
    public bool BlockInput = false;
    // Rainbow Block
    public int rainbowCountDown = 0;
    public List<int> RainbowIndexLst = new List<int>(); // List of rainbowIndex from 0 ~ 3 (Index of block)
    public List<int> BlockHasRainbowLst = new List<int>(); // List of block constain rainbow block from 0 ~ 3 (swaming block, preview block, preview_1, preview_2)

    public GameObject TetrimoPartPreb_Black;
    public GameObject[] TetrimoPartList;
    public bool IsBlackHoleShowed { get; set; }

    public Animation2DEffect blackHoleAnimationScript;
    Vector3 firstLocalScaleOfBlackHole = Vector3.zero;

    int combo = 1;
    public int Combo { get { return combo; } }
    int maxTimeToRotation = 1;

    [SerializeField]
    bool multicombo;
    public bool MultiCombo { get { return multicombo; } }

    private static GameController _instance;
    public static GameController Instance
    {
        get { return _instance; }
    }

    // Use this for initialization
    void Start()
    {
        Debug.Log("GameCont start");
        _instance = this;
        int random = Random.Range(1, 3);
        if (random == 1)
            SoundManager.Instance.PlayMusic(musicSource1, true);
        else SoundManager.Instance.PlayMusic(musicSource2, true);

        maxTimeToRotation = GameSettings.TurnToRotate;
        timeToRotate = maxTimeToRotation;
        uiManager.SetTurnToRotate(timeToRotate);
        gameScore = 0;
        ChangeScore();
        isShowGhostTetris = GameSettings.isShowGhostTetris;
        IsBlackHoleShowed = false;
        AddBlackHole();
        firstLocalScaleOfBlackHole = blackHoleAnimationScript.transform.localScale;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ChangeScore()
    {
        uiManager.ChangeTheScore(gameScore);
    }

    public void SetGameEnd()
    {
        isGameEnd = true;
        Debug.Log("Set end game with score: " + gameScore);
        PlayerDataManager.Instance.AddHighScore(gameScore);
        uiManager.ShowEndGame(gameScore);
    }

    public void StartNewGame()
    {
        Application.LoadLevel("Match");
    }

    public void CountDownTimeToRotation(bool isReSet = false)
    {
        if (isReSet)
            timeToRotate = maxTimeToRotation;
        // Neu ma vua bam HOLD thi ko can tru di
        else if (holdCountDown == 0)
            timeToRotate--;

        uiManager.SetTurnToRotate(timeToRotate);
    }

    public void ShowCombo(int index)
    {
        uiManager.ShowCombo(index, combo);
    }

    public void UpdateScore(int totalBlock)
    {
        int addScore = totalBlock;
        if (combo > 1)
            addScore *= 4 * Mathf.RoundToInt(Mathf.Pow(2, (combo - 1)));
        gameScore += addScore;

        ChangeScore();

        combo += 1;
    }

    public void UpdateMoveScore(int moveScore)
    {
        if (moveScore > 0)
        {
            gameScore += moveScore;
            ChangeScore();
        }
    }

    public void ResetCombo()
    {
        combo = 1;
    }

    void AddBlackHole()
    {
        /*GameObject[] objs = new GameObject[] {
            InitTetrimoObj(TetrimoPartPreb_Black, new Vector3(-0.5f, -0.5f, 0)),
            InitTetrimoObj(TetrimoPartPreb_Black, new Vector3(-0.5f, 0.5f, 0)),
            InitTetrimoObj(TetrimoPartPreb_Black, new Vector3(0.5f, 0.5f, 0)),
            InitTetrimoObj(TetrimoPartPreb_Black, new Vector3(0.5f, -0.5f, 0))
        };*/
        Tetrimo.SetBlock22ToBlackHole(null);
        IsBlackHoleShowed = true;
    }

    GameObject InitTetrimoObj(GameObject _obj, Vector3 pos, bool isLocal = true)
    {
        GameObject tetrimoObj = (GameObject)Instantiate(_obj, Vector3.zero, Quaternion.identity);
        tetrimoObj.transform.SetParent(transform);
        if (isLocal)
            tetrimoObj.transform.localPosition = pos;
        else
            tetrimoObj.transform.position = pos;
        return tetrimoObj;
    }

    public void ShowBlackHoleAnimation(int resizeValue)
    {
        //blackHoleAnimationScript.transform.localScale = firstLocalScaleOfBlackHole * resizeValue;
        //blackHoleAnimationScript.PlayAnimation();
    }

    public void DoBoardRotation(bool isRightRotation, System.Action<bool> callback)
    {
        transform.DORotate(new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y, transform.localEulerAngles.z + (isRightRotation ? -90f : 90f)), 1f).OnComplete(() =>
        {
            if (callback != null)
            {
                callback(isRightRotation);
            }
        });
    }

    public void isMultiCombo()
    {
        Debug.Log("MultiCombo!");
        multicombo = true;
    }

    public void isNotMultiCombo()
    {
        if (multicombo)
        {
            Debug.Log("MultiComboEnd");
            multicombo = false;
        }
    }

    #region Item functions
    public void AddImportExportBlock(bool isImport)
    {
        ImportExportItem.Instance.ShowImportExportBlock(isImport);
    }

    public void AddOrRemoveBlock(int xIndex, int yIndex, bool isImport)
    {
        Vector3 blockPos = GetPositionOfBlock(xIndex, yIndex);
        // Debug.Log("AddOrRemoveBlock: "+ blockPos.ToString() + ", " + xIndex + ", " +yIndex+", "+ isImport + ", "+ transform.childCount);
        if (isImport)
        {
            // Add a block to (xIndex, yIndex)
            GameObject _addedObj = InitTetrimoObj(TetrimoPartList[Random.Range(0, TetrimoPartList.Length)], blockPos, false);
            Tetrimo.AddOrRemoveBlock(xIndex, yIndex, _addedObj);
        }
        else
        {
            // Remove a block from (xIndex, yIndex)
            for (int i = 0; i < transform.childCount; i++)
            {
                if (transform.GetChild(i).name.Equals("BlackHoleAnimation") || transform.GetChild(i).name.Equals("GameWindow"))
                    continue;
                Vector3 childPos = transform.GetChild(i).position;
                if (Mathf.RoundToInt(childPos.x - blockPos.x) == 0 && Mathf.RoundToInt(childPos.y - blockPos.y) == 0)
                {
                    // Debug.Log("Destroy block: "+ transform.GetChild(i).gameObject.name);
                    Destroy(transform.GetChild(i).gameObject);
                    break;
                }
            }
            Tetrimo.AddOrRemoveBlock(xIndex, yIndex, null);
        }
    }

    public Vector3 GetLocalPositionOfBlock(int xIndex, int yIndex)
    {
        float xPos = -9.5f + yIndex;
        float yPos = xIndex >= 0 ? (9.5f - (9 - xIndex)) : (-0.5f + xIndex + 1);
        return new Vector3(xPos, yPos, 0);
    }

    public Vector3 GetPositionOfBlock(int xIndex, int yIndex)
    {
        return new Vector3(yIndex, xIndex, 0);
    }
    #endregion

    #region Check make rainbow

    public void CheckMakeRainbow()
    {
        rainbowCountDown--;
        if (rainbowCountDown <= 0)
        {
            rainbowCountDown = Configuration.RAINBOW_COUNTDOWN;
            BlockHasRainbowLst.Add(4);
            RainbowIndexLst.Add(Random.Range(0, 4));
        }

        List<int> removedIndex = new List<int>();
        for (int i = 0; i < BlockHasRainbowLst.Count; i++)
        {
            BlockHasRainbowLst[i] = BlockHasRainbowLst[i] - 1;
            if (BlockHasRainbowLst[i] < 0)
                removedIndex.Add(i);
        }

        for (int i = removedIndex.Count - 1; i >= 0; i--)
        {
            BlockHasRainbowLst.RemoveAt(removedIndex[i]);
            RainbowIndexLst.RemoveAt(removedIndex[i]);
        }
    }

    #endregion

    public bool isGamePauseTest = false;
    public int ShapeIndexTest = 4;
    void OnGUI()
    {
        /*if(GUILayout.Button(isGamePauseTest.ToString()))
        {
            isGamePauseTest = !isGamePauseTest;
        }*/
    }
}
