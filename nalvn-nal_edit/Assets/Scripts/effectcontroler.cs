﻿using UnityEngine;
using System.Collections;

public class effectcontroler : MonoBehaviour {

    ParticleSystem paricleSystem;

    [SerializeField]
    bool autoDelete;

    float times = 0;
    float count;

	//ここではパーティクルシステムを制御します
	void Start ()
    {
       paricleSystem = transform.FindChild("Particle System").gameObject.GetComponent<ParticleSystem>();
	}
	
	void Update ()
    {
        if (autoDelete)
        {
            if (!paricleSystem.IsAlive())
            {
                Destroy(this.gameObject);
            }
        }
	
        times += Time.deltaTime;

        if (times > paricleSystem.startLifetime)
        {
            count++;
            transform.localEulerAngles = new Vector3(0, 0, 5 * count);
        }
	}
}
