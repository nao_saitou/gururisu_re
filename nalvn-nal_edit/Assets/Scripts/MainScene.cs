﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class MainScene : MonoBehaviour {
	public Transform panel;
	public Transform logo;
	bool isLoading = false;
    float logoRotationZ = 180;
    float delayRotateTime = 5f;
	public AudioClip mainSceneMusic;
	
    // Use this for initialization
	void Start () {
		SoundManager.Instance.PlayMusic (mainSceneMusic, true);

		panel.localScale = Vector3.zero;
		iTween.ScaleTo(panel.gameObject, Vector3.one, 1f);
        logoRotationZ = 180;
        delayRotateTime = 2f;
        //DoLogoRotate();
	}
	
	// Update is called once per frame
	void Update () {
	}

	public void OnMenuEvent(string sceneName)
	{
		if(isLoading == false && !string.IsNullOrEmpty(sceneName))
		{
			isLoading = true;
			Application.LoadLevel(sceneName);
		}
	}

    void DoLogoRotate()
    {
        iTween.RotateTo(logo.gameObject, iTween.Hash("z", logoRotationZ, "islocal", true, "time", 5f, "delay", delayRotateTime, 
            "easetype", "linear", "oncompletetarget", this.gameObject, "oncomplete", "DoLogoRotate"));
        
        if (logoRotationZ == 180)
        {
            logoRotationZ = 360;
            delayRotateTime = 0f;
        }
        else
        {
            logoRotationZ = 180;
            delayRotateTime = 5f;
        }
    }
}
