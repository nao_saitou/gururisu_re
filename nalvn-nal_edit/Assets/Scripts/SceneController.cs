﻿using UnityEngine;
using System.Collections;
using System;

public class SceneController : MonoBehaviour {

    public string previousScene;
    public Action callback;
    public bool blockOnCallBack = false;
    bool onQuitScene = false;
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            DoBack();
    }

    void DoBack()
    {
        if (onQuitScene || blockOnCallBack)
            return;

        if (callback != null)
            callback();
        else
        {
            onQuitScene = true;
            if (!string.IsNullOrEmpty(previousScene))
                Application.LoadLevel(previousScene);
            else
                Application.Quit();
        }
    }
}
