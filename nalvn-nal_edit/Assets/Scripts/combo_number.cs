﻿using UnityEngine;
using System.Collections;

public class combo_number : MonoBehaviour {

    [SerializeField]
    Sprite[] number = new Sprite[10];

    public int COMBO;

	void Start ()
    {
        if (COMBO != 0)
        {
            if (COMBO < 10)
            {
                GameObject keta1 = Instantiate(Resources.Load("effect/combo_number"), new Vector3(10, -4, -6), Quaternion.identity) as GameObject;

                keta1.name = "combo_number1";
                keta1.GetComponent<SpriteRenderer>().sprite = number[COMBO];
            }
            if (COMBO >= 10)
            {
                GameObject keta1 = Instantiate(Resources.Load("effect/combo_number"), new Vector3(9, -4, -6), Quaternion.identity) as GameObject;
                GameObject keta2 = Instantiate(Resources.Load("effect/combo_number"), new Vector3(11, -4, -6), Quaternion.identity) as GameObject;

                keta1.name = "combo_number1";
                keta2.name = "combo_number2";

                int buf1 = 0;
                int buf2 = 0;

                buf1 = (int)(COMBO * 0.1) % 10;

                if (buf1 >= 0 && buf1 <= 9)//画像の有効範囲内であることを保証する
                {
                    keta1.GetComponent<SpriteRenderer>().sprite = number[buf1];
                }

                buf2 = (int)(COMBO) % 10;

                if (buf2 >= 0 && buf2 <= 9)//画像の有効範囲内であることを保証する
                {
                    keta2.GetComponent<SpriteRenderer>().sprite = number[buf2];
                }
            }
            if (COMBO >= 99)
            {
                GameObject keta1 = Instantiate(Resources.Load("effect/combo_number"), new Vector3(9, -4, -6), Quaternion.identity) as GameObject;
                GameObject keta2 = Instantiate(Resources.Load("effect/combo_number"), new Vector3(11, -4, -6), Quaternion.identity) as GameObject;

                keta1.name = "combo_number1";
                keta2.name = "combo_number2";

                keta1.GetComponent<SpriteRenderer>().sprite = number[9];
                keta2.GetComponent<SpriteRenderer>().sprite = number[9];
            }
            Instantiate(Resources.Load("effect/combo_img"), new Vector3(17, -4, -6), Quaternion.identity);
        }
        else
        {
            Destroy(this.gameObject);
        }
	}
}
