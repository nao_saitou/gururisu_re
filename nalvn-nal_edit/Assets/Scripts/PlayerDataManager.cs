﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class PlayerData
{
	public string playDate;
	public long score;

	public override string ToString ()
	{
		return string.Format ("{0};and;{1}",playDate,score);
	}
}

public class PlayerDataManager : MonoBehaviour {
    public int GPoint { get; private set; }
	public int maxRecord = 5;
	private static PlayerDataManager _instance;
	public static PlayerDataManager Instance
	{
		get 
		{
			if(_instance == null)
			{
				GameObject obj = new GameObject("PlayerDataManager");
				_instance = obj.AddComponent<PlayerDataManager>();
			}
			return _instance;
		}
	}

	public List<PlayerData> playerDataLst;

	void Awake () {
		if (_instance == null)
			_instance = this;
		DontDestroyOnLoad (gameObject);
		GetPlayerData ();
        GPoint = PlayerPrefs.GetInt("PlayerGPoint", Configuration.START_GPOINT);
	}

    public void ChangeGPoint(int _gpoint)
    {
        int newGPoint = GPoint + _gpoint;
        GPoint = Mathf.Max(newGPoint, 0);
        PlayerPrefs.SetInt("PlayerGPoint", GPoint);
        SimpleLibs.Messenger.Broadcast("UpdateGPointUI", SimpleLibs.MessengerMode.DONT_REQUIRE_LISTENER);
    }

	void GetPlayerData()
	{
		playerDataLst = new List<PlayerData> ();
		string[] datas = PlayerPrefsX.GetStringArray ("LocalPlayerData");
		for (int i=0; i<datas.Length; i++) {
			PlayerData player = new PlayerData();
			string[] tempArr = datas[i].Split(new string[] {";and;"},  System.StringSplitOptions.None);
			if(tempArr.Length == 2)
			{
				player.playDate = tempArr[0];
				player.score = int.Parse(tempArr[1]);
				playerDataLst.Add(player);
			}
		}
	}

	public void SetPlayerData(PlayerData data)
	{
		playerDataLst.Add (data);
		if(playerDataLst.Count > 1)
		playerDataLst.Sort ((p1, p2) => {
			return p2.score.CompareTo(p1.score);
		});

		if (playerDataLst.Count > maxRecord) {
			playerDataLst.RemoveAt(maxRecord);
		}

		string[] datas = new string[playerDataLst.Count];
		for (int i=0; i<datas.Length; i++) {
			datas[i] = playerDataLst[i].ToString();
		}

		PlayerPrefsX.SetStringArray ("LocalPlayerData", datas);
	}

	public void AddHighScore(long score)
	{
        if (score > 0)
        {
            PlayerData data = new PlayerData();
            data.playDate = Utilities.GetCurrentDateJapanese();
            data.score = score;
            SetPlayerData(data);
        }
	}

	public void ResetPlayerData()
	{
		playerDataLst = new List<PlayerData> ();
		PlayerPrefs.DeleteKey ("LocalPlayerData");
	}

	// Update is called once per frame
	void Update () {
	
	}
}
