﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RecordScene : MonoBehaviour {
	public GameObject panel;
	public AudioClip clip;
	bool isBackBtnPress = false;
	public GameObject recordGrid;
	int maxRecord = 5;
	List<GameObject> recordObjLst;
	
    void Start () {
		panel.transform.localScale = Vector3.zero;
		iTween.ScaleTo(panel, Vector3.one, 1f);
		SoundManager.Instance.PlayMusic (clip, true);
		SetupData ();
	}
	
	void SetupData()
	{
		if (recordObjLst != null && recordObjLst.Count > 0) {
			for(int i=0; i<recordObjLst.Count; i++)
			{
				recordObjLst[i].transform.SetParent(null);
				Destroy(recordObjLst[i]);
			}
		}
		recordObjLst = new List<GameObject> ();
		List<PlayerData> datas = new List<PlayerData> (PlayerDataManager.Instance.playerDataLst);
		for (int i=0; i<maxRecord; i++) {
			GameObject obj = (GameObject)Instantiate((GameObject)Resources.Load("RecordObject"));
			obj.name = "Record_"+i;
			if(datas.Count > i)
			{
				obj.GetComponent<RecordObject>().SetText((i+1).ToString(), datas[i].playDate, datas[i].score.ToString());
			}
			else
			{
				obj.GetComponent<RecordObject>().SetText((i+1).ToString(), "成績がありません。", "");
			}
			obj.transform.SetParent(recordGrid.transform);
			obj.transform.localScale = Vector3.one;
			recordObjLst.Add(obj);
		}
	}

	public void OnButtonEvent(string eventName)
	{
		if (isBackBtnPress)
			return;
		
		switch (eventName) {
		case "back":
			isBackBtnPress = true;
			iTween.ScaleTo (panel, iTween.Hash ("scale", Vector3.zero, "time", 1f, "oncompletetarget", this.gameObject, "oncomplete", "BackToMainMenu"));
			break;

		case "reset":
            AlertManager.Instance.ShowAlert("成績を初期化します。よろしいですか？", gameObject, "ConfirmResetRecordYes");
			break;
		}
	}

	void BackToMainMenu()
	{
		Application.LoadLevel ("MainScene");
	}

    void ConfirmResetRecordYes()
    {
        PlayerDataManager.Instance.ResetPlayerData();
        SetupData();
    }

}
