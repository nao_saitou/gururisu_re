﻿using UnityEngine;
using System.Collections;

public class GameSettings {

	// So turn de quay matrix
	public static int TurnToRotate
	{
		set { PlayerPrefs.SetInt("TurnToRotate", value); }
		get { return PlayerPrefs.GetInt("TurnToRotate", 2); }
	}

	// Enable/Disable sound/music
	public static bool isSoundEnabled
	{
		set { PlayerPrefs.SetInt("isSoundEnabled", (value == true ? 1 : 0)); }
		get { return (PlayerPrefs.GetInt("isSoundEnabled", 1) == 1 ? true : false); }
	}

	// An/Hien vi tri ma block se di chuyen xuong duoi
	public static bool isShowGhostTetris
	{
		set { PlayerPrefs.SetInt("isShowGhostTetris", (value == true ? 1 : 0)); }
		get { return (PlayerPrefs.GetInt("isShowGhostTetris", 1) == 1 ? true : false); }
	}
}
