﻿using UnityEngine;
using System.Collections;
using SimpleLibs;
public class ImportExportItem : MonoBehaviour {
    static ImportExportItem _instance;
    public static ImportExportItem Instance { get { return _instance; } }

    public bool isImport = true;
    int curXIndex = 9;
    int curYIndex = 0;
    public int maxXIndex = 10;
    public int maxYIndex = 20;

    public Transform blockObj;
    public GameObject buttonObj;
    public GameObject rotateObj;
    public int blockSpace;

    void Awake()
    {
        _instance = this;
        blockObj.gameObject.SetActive(false);
        buttonObj.SetActive(false);
    }

    void OnMoveClick(string type)
    {
        switch (type)
        {
            case "left":
                if(curYIndex > 0)
                {
                    curYIndex -= 1;
                }
                break;

            case "right":
                if(curYIndex < maxYIndex - 1)
                {
                    curYIndex += 1;
                }
                break;

            case "down":
                if(curXIndex > -maxXIndex)
                {
                    curXIndex -= 1;
                }
                break;

            case "up":
                if (curXIndex < maxXIndex - 1)
                {
                    curXIndex += 1;
                }
                break;
        }
        blockObj.localPosition = GameController.Instance.GetLocalPositionOfBlock(curXIndex, curYIndex);
    }

    public void OnButtonEvent(string eventName)
    {
        switch (eventName)
        {
            case "ok":
                if (!Tetrimo.IsTetrimoBlock(curXIndex, curYIndex) && !Tetrimo.IsBlackHoleBlock(curXIndex, curYIndex) && 
                    ((isImport && Tetrimo.isEmptyBlock(curXIndex, curYIndex)) || (!isImport && !Tetrimo.isEmptyBlock(curXIndex, curYIndex))))
                {
                    string titleStr = (isImport ? "ブロックを置きます。" : "ブロックを取り除きます。") + "よろしいですか？";
                    AlertManager.Instance.ShowAlert(titleStr, gameObject, "OnOkConfirmed");
                }
                break;

            case "cancel":
                GameController.Instance.isGamePause = false;
                HideImportExportBlock();
                break;
        }
    }

    public void ShowImportExportBlock(bool _isImport)
    {
        isImport = _isImport;
        curXIndex = maxXIndex - 1;
        curYIndex = 0;

        // Move block object to top left of matrix
        blockObj.gameObject.SetActive(true);
        blockObj.localPosition = GameController.Instance.GetLocalPositionOfBlock(curXIndex, curYIndex);

        buttonObj.SetActive(true);
        rotateObj.SetActive(false);

        Messenger<string>.AddListener("OnMoveClick", OnMoveClick);
        GameController.Instance.BlockInput = true;
    }

    void HideImportExportBlock()
    {
        blockObj.gameObject.SetActive(false);
        buttonObj.SetActive(false);
        rotateObj.SetActive(true);
        Messenger<string>.RemoveListener("OnMoveClick", OnMoveClick);
        GameController.Instance.BlockInput = false;
    }

    void OnOkConfirmed()
    {
        GameController.Instance.isGamePause = false;
        GameController.Instance.AddOrRemoveBlock(curXIndex, curYIndex, isImport);
        HideImportExportBlock();
    }
}
