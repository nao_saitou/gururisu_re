﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Animation2DEffect : MonoBehaviour {
    public string pathFormat = "";
    public int startIndex = 0;
    public int endIndex = 1;

    SpriteRenderer mainSprite;
    public List<Sprite> animatedSprites;

    public bool isAutodestroy = true;
    public bool isStopWhenDone = false;
    public bool isAutoPlayWhenAwake = true;
    public enum AnimationType { ByFrames, BySeconds}
    public float animateTimeBySecond = 0.001f;
    public AnimationType animationType = AnimationType.ByFrames;

    float runningTime = 0;
    int spriteIndex = 0;
    bool isPlayAnimation = true;
    void Start()
    {
		// Add all image of animation to list
        if (animatedSprites == null || (animatedSprites != null && animatedSprites.Count == 0))
        {
            animatedSprites = new List<Sprite>();
            for (int i = startIndex; i <= endIndex; i++)
            {
                Sprite tmp = Resources.Load<Sprite>(string.Format(pathFormat, i));
                if (tmp != null)
                {
                    animatedSprites.Add(tmp);
                }
                else
                {
                    Debug.Log("temp == null - " + string.Format(pathFormat, i));
                }
            }
        }

        if (animatedSprites.Count == 0)
            Destroy(gameObject);
        else
        {
            mainSprite = GetComponent<SpriteRenderer>();
            mainSprite.sprite = animatedSprites[0];
            spriteIndex = 1;

            if (isAutoPlayWhenAwake == false)
                StopAnimation();
        }
    }

    void Update()
    {
        if (isPlayAnimation == false)
            return;

		// Call change image function to create animation ( 0.001f second change 1 image)
        if(animationType == AnimationType.ByFrames)
        {
            SetNewSprite();
        } else if(animationType == AnimationType.BySeconds)
        {
            runningTime += Time.deltaTime;
            if(runningTime >= animateTimeBySecond)
            {
                SetNewSprite();
                runningTime = 0;
            }
        }
    }
	// This function will change image in animation. The image changes will create animation
    void SetNewSprite()
    {
        mainSprite.sprite = animatedSprites[spriteIndex];
        spriteIndex++;
        if (spriteIndex >= animatedSprites.Count)
        {
            spriteIndex = 0;
            if (isAutodestroy)
                Destroy(gameObject);
            if (isStopWhenDone)
                StopAnimation();
        }
    }
	// This function call when want stop animation
    public void StopAnimation()
    {
        mainSprite.enabled = false;
        isPlayAnimation = false;
    }

	// This function call when want play animation
    public void PlayAnimation()
    {
        mainSprite.enabled = true;
        isPlayAnimation = true;
        mainSprite.sprite = animatedSprites[0];
        spriteIndex = 1;
    }
}
