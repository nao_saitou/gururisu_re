﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class SettingsScene : MonoBehaviour {
	public GameObject panel;

	public Sprite onBtnBg;
	public Sprite offBtnBg;

	public Image ghostShownOnBg;
	public Image ghostShownOffBg;
	public Image soundShownOnBg;
	public Image soundShownOffBg;
	public AudioClip clip;
	public GameObject turnToRotateLst;
	public Text turnToRotateText;

	bool isBackBtnPress = false;

	// Use this for initialization
	void Start () {
		panel.transform.localScale = Vector3.zero;
		iTween.ScaleTo(panel, Vector3.one, 1f);
		SoundManager.Instance.PlayMusic (clip, true);
		SetupBtnBg ();
		turnToRotateText.text = GameSettings.TurnToRotate.ToString ();
		turnToRotateLst.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnButtonEvent(string eventName)
	{
		if (isBackBtnPress)
			return;

		switch (eventName) {
		case "back":
			isBackBtnPress = true;
			iTween.ScaleTo(panel, iTween.Hash("scale", Vector3.zero, "time", 1f, "oncompletetarget", this.gameObject, "oncomplete", "BackToMainMenu"));
			break;

		case "music_on":
			if(GameSettings.isSoundEnabled == false)
			{
				SoundManager.Instance.RestartMusic();
				GameSettings.isSoundEnabled = true;
				SetupBtnBg();
			}
			break;

		case "music_off":
			if(GameSettings.isSoundEnabled == true)
			{
				SoundManager.Instance.StopMusic();
				GameSettings.isSoundEnabled = false;
				SetupBtnBg();
			}
			break;

		case "ghost_on":
			if(GameSettings.isShowGhostTetris == false)
			{
				GameSettings.isShowGhostTetris = true;
				SetupBtnBg();
			}
			break;

		case "ghost_off":
			if(GameSettings.isShowGhostTetris == true)
			{
				GameSettings.isShowGhostTetris = false;
				SetupBtnBg();
			}
			break;
		}
	}

	void SetupBtnBg()
	{
		if (GameSettings.isShowGhostTetris) {
			ghostShownOnBg.sprite = onBtnBg;
			ghostShownOffBg.sprite = offBtnBg;
		} else {
			ghostShownOnBg.sprite = offBtnBg;
			ghostShownOffBg.sprite = onBtnBg;
		}
		
		if (GameSettings.isSoundEnabled) {
			soundShownOnBg.sprite = onBtnBg;
			soundShownOffBg.sprite = offBtnBg;
		} else {
			soundShownOnBg.sprite = offBtnBg;
			soundShownOffBg.sprite = onBtnBg;
		}
	}

	void BackToMainMenu()
	{
		Application.LoadLevel ("MainScene");
	}

	bool isTurnToRotateScale = false;
	bool isHide = false;
	public void OnTurnToRotateBtnEvent(int _id)
	{
		if (isTurnToRotateScale == false) {
			isTurnToRotateScale = true;
			if (_id == -1) {
				if (turnToRotateLst.activeSelf) {
					isHide = true;
					iTween.ScaleTo (turnToRotateLst, iTween.Hash ("scale", new Vector3(1,0,1), "time", 0.5f, "oncompletetarget", this.gameObject, "oncomplete", "OnTurnToRotateScaleFinish"));
				} else {
					isHide = false;
					turnToRotateLst.SetActive (true);
					turnToRotateLst.transform.localScale = new Vector3(1,0,1);
					iTween.ScaleTo (turnToRotateLst, iTween.Hash ("scale", Vector3.one, "time", 0.5f, "oncompletetarget", this.gameObject, "oncomplete", "OnTurnToRotateScaleFinish"));
				}
			} else {
				turnToRotateText.text = _id.ToString ();
				GameSettings.TurnToRotate = _id;
				isHide = true;
				iTween.ScaleTo (turnToRotateLst, iTween.Hash ("scale", new Vector3(1,0,1), "time", 0.5f, "oncompletetarget", this.gameObject, "oncomplete", "OnTurnToRotateScaleFinish"));
			}
		}
	}

	void OnTurnToRotateScaleFinish()
	{
		isTurnToRotateScale = false;
		if(isHide)
			turnToRotateLst.SetActive (false);
	}
}
