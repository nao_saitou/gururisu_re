using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class InAppPurchaseManager : MonoBehaviour {
	private static InAppPurchaseManager _instance;
	public static InAppPurchaseManager Instance { get { return _instance; } }

	void Awake()
	{
		if (Application.platform != RuntimePlatform.Android && Application.platform != RuntimePlatform.IPhonePlayer) {
			Destroy (gameObject);
			return;
		}
        if (_instance == null)
        {
            Debug.Log("Init IAB");
            _instance = this;
            DontDestroyOnLoad(gameObject);
#if UNITY_ANDROID
            InitIAB();
#elif UNITY_IPHONE
		    StoreKitBinding.requestProductData( Configuration.GOOGLE_SKUS );
#endif
        }
        else
        {
            Destroy(gameObject);
        }
	}

	#region public methods
	public void BuyItem(string sku)
	{
#if UNITY_ANDROID
		GoogleIAB.purchaseProduct(sku);
#elif UNITY_IPHONE
		StoreKitBinding.purchaseProduct(sku, 1);
#endif
	}
	#endregion

#if UNITY_ANDROID
	void InitIAB()
	{
		GoogleIAB.init( Configuration.IAB_PUBLIC_KEY );
	}

	void OnEnable()
	{
		// Listen to all events for illustration purposes
		GoogleIABManager.billingSupportedEvent += billingSupportedEvent;
		GoogleIABManager.billingNotSupportedEvent += billingNotSupportedEvent;
		GoogleIABManager.queryInventorySucceededEvent += queryInventorySucceededEvent;
		GoogleIABManager.queryInventoryFailedEvent += queryInventoryFailedEvent;
		GoogleIABManager.purchaseCompleteAwaitingVerificationEvent += purchaseCompleteAwaitingVerificationEvent;
		GoogleIABManager.purchaseSucceededEvent += purchaseSucceededEvent;
		GoogleIABManager.purchaseFailedEvent += purchaseFailedEvent;
		GoogleIABManager.consumePurchaseSucceededEvent += consumePurchaseSucceededEvent;
		GoogleIABManager.consumePurchaseFailedEvent += consumePurchaseFailedEvent;
	}
	
	
	void OnDisable()
	{
		// Remove all event handlers
		GoogleIABManager.billingSupportedEvent -= billingSupportedEvent;
		GoogleIABManager.billingNotSupportedEvent -= billingNotSupportedEvent;
		GoogleIABManager.queryInventorySucceededEvent -= queryInventorySucceededEvent;
		GoogleIABManager.queryInventoryFailedEvent -= queryInventoryFailedEvent;
		GoogleIABManager.purchaseCompleteAwaitingVerificationEvent += purchaseCompleteAwaitingVerificationEvent;
		GoogleIABManager.purchaseSucceededEvent -= purchaseSucceededEvent;
		GoogleIABManager.purchaseFailedEvent -= purchaseFailedEvent;
		GoogleIABManager.consumePurchaseSucceededEvent -= consumePurchaseSucceededEvent;
		GoogleIABManager.consumePurchaseFailedEvent -= consumePurchaseFailedEvent;
	}
	
	
	
	void billingSupportedEvent()
	{
		Debug.Log( "billingSupportedEvent" );
		GoogleIAB.queryInventory (Configuration.GOOGLE_SKUS);
	}
	
	
	void billingNotSupportedEvent( string error )
	{
		Debug.Log( "billingNotSupportedEvent: " + error );
	}
	
	
	void queryInventorySucceededEvent( List<GooglePurchase> purchases, List<GoogleSkuInfo> skus )
	{
		Debug.Log( string.Format( "queryInventorySucceededEvent. total purchases: {0}, total skus: {1}", purchases.Count, skus.Count ) );
		Prime31.Utils.logObject( purchases );
		Prime31.Utils.logObject( skus );
	}
	
	
	void queryInventoryFailedEvent( string error )
	{
		Debug.Log( "queryInventoryFailedEvent: " + error );
	}
	
	
	void purchaseCompleteAwaitingVerificationEvent( string purchaseData, string signature )
	{
		Debug.Log( "purchaseCompleteAwaitingVerificationEvent. purchaseData: " + purchaseData + ", signature: " + signature );
	}
	
	
	void purchaseSucceededEvent( GooglePurchase purchase )
	{
        for (int i = 0; i < Configuration.GOOGLE_SKUS.Length; i++)
        {
            if (purchase.productId.Equals(Configuration.GOOGLE_SKUS[i]))
            {
                PlayerDataManager.Instance.ChangeGPoint(Configuration.GPOINT_ARRAY[i]);
                break;
            }
        }
		GoogleIAB.consumeProduct (purchase.productId);
		Debug.Log( "purchaseSucceededEvent: " + purchase );
	}
	
	
	void purchaseFailedEvent( string error, int response )
	{
		Debug.Log( "purchaseFailedEvent: " + error + ", response: " + response );
	}
	
	
	void consumePurchaseSucceededEvent( GooglePurchase purchase )
	{
		Debug.Log( "consumePurchaseSucceededEvent: " + purchase );
	}
	
	
	void consumePurchaseFailedEvent( string error )
	{
		Debug.Log( "consumePurchaseFailedEvent: " + error );
	}

#endif

#if UNITY_IPHONE
	public void OnEnable()
	{
		StoreKitBinding.finishPendingTransactions ();
		StoreKitManager.productPurchaseAwaitingConfirmationEvent += productPurchaseAwaitingConfirmationEvent;
		StoreKitManager.purchaseSuccessfulEvent += purchaseSuccessful;
		StoreKitManager.purchaseCancelledEvent += purchaseCancelled;
		StoreKitManager.purchaseFailedEvent += purchaseFailed;
		StoreKitManager.productListReceivedEvent += productListReceivedEvent;
		StoreKitManager.productListRequestFailedEvent += productListRequestFailed;
		StoreKitManager.restoreTransactionsFailedEvent += restoreTransactionsFailed;
		StoreKitManager.restoreTransactionsFinishedEvent += restoreTransactionsFinished;
		StoreKitManager.paymentQueueUpdatedDownloadsEvent += paymentQueueUpdatedDownloadsEvent;
	}
	
	
	public void OnDisable()
	{
		StoreKitManager.productPurchaseAwaitingConfirmationEvent -= productPurchaseAwaitingConfirmationEvent;
		StoreKitManager.purchaseSuccessfulEvent -= purchaseSuccessful;
		StoreKitManager.purchaseCancelledEvent -= purchaseCancelled;
		StoreKitManager.purchaseFailedEvent -= purchaseFailed;
		StoreKitManager.productListReceivedEvent -= productListReceivedEvent;
		StoreKitManager.productListRequestFailedEvent -= productListRequestFailed;
		StoreKitManager.restoreTransactionsFailedEvent -= restoreTransactionsFailed;
		StoreKitManager.restoreTransactionsFinishedEvent -= restoreTransactionsFinished;
		StoreKitManager.paymentQueueUpdatedDownloadsEvent -= paymentQueueUpdatedDownloadsEvent;
	}

	void productListReceivedEvent( List<StoreKitProduct> productList )
	{
		string messageStr = "";
		foreach( StoreKitProduct product in productList )
		{
			messageStr += product.ToString() + "\n";
		}
	}
	
	
	void productListRequestFailed( string error )
	{

	}
	
	
	void purchaseFailed( string error )
	{
		Debug.Log ("Purchase failed");
	}
	
	
	void purchaseCancelled( string error )
	{
		Debug.Log ("Purchase cancelled");
	}
	
	
	void productPurchaseAwaitingConfirmationEvent( StoreKitTransaction transaction )
	{
	}
	
	
	void purchaseSuccessful( StoreKitTransaction transaction )
	{
		if(transaction != null)
		{
            for (int i = 0; i < Configuration.GOOGLE_SKUS.Length; i++)
            {
                if (transaction.productIdentifier.Equals(Configuration.GOOGLE_SKUS[i]))
                {
                    PlayerDataManager.Instance.ChangeGPoint(Configuration.GPOINT_ARRAY[i]);
                    break;
                }
            }
		}
	}
	
	void restoreTransactionsFailed( string error )
	{

	}
	
	void restoreTransactionsFinished()
	{

	}
	
	void paymentQueueUpdatedDownloadsEvent( List<StoreKitDownload> downloads )
	{

	}
#endif
}
