﻿using UnityEngine;
using System.Collections;

public class MojiEffectCont : MonoBehaviour
{
    [SerializeField]
    bool autoDelete;

    [SerializeField]
    float DeleteTime;

    GameController gc;

	void Start ()
    {
        gc = GameObject.Find("GameController").GetComponent<GameController>();
	}
	
	void Update ()
    {
        if (autoDelete)
        {
            if (DeleteTime <= 0.0f)
            {
                gc.isNotMultiCombo();
                Destroy(this.gameObject);
            }
        }

        DeleteTime -= Time.deltaTime;
	}
}
