﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
public class AlertManager : MonoBehaviour {

    private static AlertManager _instance;
    public static AlertManager Instance
    {
        get
        {
            if(_instance == null)
            {
                GameObject obj = (GameObject)Instantiate((GameObject)Resources.Load("AlertBox"));
                obj.name = "__AlertBox";
                _instance = obj.transform.FindChild("AlertManager").GetComponent<AlertManager>();
            }
            return _instance;
        }
    }

    public GameObject panel;
    public Text titleText;
    public Text yesBtnText;
    public Text noBtnText;
    public string defaultYesStr;
    public string defaultNoStr;

    GameObject target;
    string yesMethod;
    string noMethod;

    void Awake()
    {
        if (_instance == null)
            _instance = this;
    }

    public void OnButtonEvent(string eventName)
    {
        switch(eventName)
        {
            case "yes":
                ClosePopupAnimation("OnYesEvent");
                break;

            case "no":
                ClosePopupAnimation("OnNoEvent");
                break;
        }
    }

    public void ShowAlert(string title, string yes, string no, GameObject objReceive, string yesMethodName, string noMethodName)
    {
        panel.SetActive(true);
        panel.transform.localScale = Vector3.zero;
        iTween.ScaleTo(panel, Vector3.one, 0.5f);
        titleText.text = title;
        yesBtnText.text = yes;
        noBtnText.text = no;
        target = objReceive;
        yesMethod = yesMethodName;
        noMethod = noMethodName;
    }

    public void ShowAlert(string title, GameObject objReceive, string yesMethodName, string noMethodName = "")
    {
        ShowAlert(title, defaultYesStr, defaultNoStr, objReceive, yesMethodName, noMethodName);
    }

    private void OnYesEvent()
    {
        panel.SetActive(false);
        if(target != null && !string.IsNullOrEmpty(yesMethod))
        {
            target.SendMessage(yesMethod);
        }
        target = null;
    }

    private void OnNoEvent()
    {
        panel.SetActive(false);
        if (target != null && !string.IsNullOrEmpty(noMethod))
        {
            target.SendMessage(noMethod);
        }
        target = null;
    }

    private void ClosePopupAnimation(string eventName)
    {
        iTween.ScaleTo(panel, iTween.Hash("scale", Vector3.zero, "time", 0.5f, "oncompletetarget", this.gameObject, "oncomplete", eventName));
    }

    void OnDestroy()
    {
        _instance = null;
    }
}
