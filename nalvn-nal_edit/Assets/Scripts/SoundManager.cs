﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour {

	public AudioSource source;

	private static SoundManager _instance;
	public static SoundManager Instance { 
		get 
		{ 
			if(_instance == null)
			{
				GameObject obj = (GameObject)Instantiate((GameObject)Resources.Load("SoundManager"));
				_instance = obj.GetComponent<SoundManager>();
			}
			return _instance; 
		} 
	}

	// Use this for initialization
	void Awake () {
		if(_instance == null)
			_instance = this;
		DontDestroyOnLoad (gameObject);
	}

	public void PlayMusic(AudioClip clip, bool isLoop)
	{
		if(source.isPlaying)
			source.Stop ();

		if (clip != null) {
			source.clip = clip;
			source.loop = isLoop;

			if (GameSettings.isSoundEnabled)
				source.Play ();
		}
	}

	public void StopMusic()
	{
		if(source.isPlaying)
			source.Stop ();	
	}

	public void RestartMusic()
	{
		if (source.clip != null)
			source.Play ();
	}
}
